# AWS Services Overview
- Main Study Resources:
  - [ACloudGuru - Solutions Architect Associate](https://www.udemy.com/course/aws-certified-solutions-architect-associate/learn/lecture/13885822?start=0#overview)
    - Very good course on covering foundational services and concepts for AWS Solutions Architect
  - [AWS Certified Solutions Architect Associate Practice Exams (5)](https://www.udemy.com/course/aws-certified-solutions-architect-associate-amazon-practice-exams-saa-c02/)
    - cannot recommend this enough, while the ACloudGuru gave me the foundation and core to get about 50%, these exams and their explanations took me the rest of the way
    - EXCELLENT explanations for each question (right or wrong) as to why the correct answer is correct and all others are not as good or incorrect
  - Alternative to ACloudGuru: [OReilly AWS Certified Solutions Architect](https://learning.oreilly.com/videos/aws-certified-solutions/9780136721246/9780136721246-ACS2_00_00_00)
    - Although I never used it, this was going to be the backup course I took. I only used the question reviews at the end of each section. 
    - Much better framed to scope of AWS cert exam than acloudguru

## AWS Topology
- Region: Geographical Area
- Region will have 2 or more Availability Zones
- Availability Zone: Discrete Data Center
- Edge Locations: End Points for Caching Content
- AWS Backbone - internal AWS networking

## Compute 

EC2 -- Elastic Cloud containing VMs or Physical Machines
Elastic Beanstalk -- developers looking for an environment to code
Lambda -- For uploading the code and control/execute it from there
Lightsail -- get a static ip 
Batch -- used for batch computing in cloud


## Storage:

S3 -- Simple Storage Service
EFS -- Elastic File System -- Network Attached Storage
Glacier -- Data Archival
Snowball -- For uploading huge data to AWS 
Storage Gateway -- store data in AWS cloud with an agent on premise


## Databases:

RDS -- Relational Database Service -- MySQL, MS SQL....
DynamoDB -- Serverless Global Non relational Databases
Elasticache -- For caching commonly queried things from database and freeing up databases
Redshift -- Datawarehousing for big data analytics


## Migration:

AWS Migration Hub --- Tracking service as you migrate to AWS
Application Discovery Service -- For finding the dependencies on Application
Database Migration Service -- For migrating on-premise database to AWS
Server Migration Service -- For migrating servers to AWS
Snowball


## Networking and Content Delivery:

VPC -- Virtual Private Cloud -- design firewalls, routing....its like Virtual Data Center
CloudFront -- Amazon's Content Delivery Network
Route53 -- DNS Service
API Gateway -- Create your own APIs
Direct Connect -- for runnig dedicated line from your Data Center to AWS VPC


## Management Tools:

CloudWatch -- "watch performance"
CloudFormation -- 
CloudTrail -- Track users and API activity "Audit Trail"
Config -- Manage snapshots and configs and track resource activity
OpsWorks -- uses chef and puppet for automation
Service Catalog
Systems Manager --- patch all EC2...
Trusted Advisor -- Advice around security, saves money with AWS
Managed Services


## Media Services:

Elastic Transcoder
Media..


## Machine Learning:

SageMaker -- used for deep learning (around neural networks)
Comprehend -- sentimental analysis
DeepLens 
Lex -- Alexa backend technology
Machine Learning -- analyze dataset and predict outcomes
Polly -- Text to speech converter
Rekognition -- audio and video recognition
Amazon Translate --
Amazon Transcribe -- extract speech from video and convert it to text


## Analytics:

Athena -- run sql queries against S3 buckets
EMR -- Elastic Map Reduce -- 
CloudSearch
ElasticSearch Service
Kinesis -- inject social media data into AWS
Kinesis Video Streams --
QuickSight -- BI Tool and less expensive
Data Pipeline -- moving data between AWS Services
Glue -- for ETL


## Security & Identity & Management:

IAM -- Identity Access Management
Cognito -- Authentication Services that gives temporary access to AWS
GuardDuty --
Inspector -- install inspector on EC2 to check vulnerabilities....
Macie -- search your S3 buckets for PII, SSNs, Credit Card Info....
Certificate Manager --
CloudHSM -- Cloud Hardware Security Model
Directory Service -- Integrating your Active Directory with AWS
WAF -- Web Application Firewall -- 
Shield -- protects from DDOS attacks
Artifact -- 
Mobile Services:



Auditing check which services

Cloud Front

# Detailed Notes from Udemy

# IAM - Identity and Access Management
- Global (not regional) 
- Programmatic or manual interface to manage users, groups, policies (Permission grants defined in JSON) and roles (hats)
  -  access key ID and secret access key for the AWS API, CLI, SDK and other development tools
  - AWS Management Console access uses username and password for logging into console
    - Can specify password policies: rotation, temporary access...
    - PCI DSS compliant
  - Allows ID federation to AD / Google... 
- Root Account is your email address signed up for AWS, should have MFA
- Policies can be applied to Users, Groups and Roles
  - Policy contain Policy Document providing formal statement of one or more permissions 

- By default user created in IAM has No Access to AWS Services - you must manually grant permissions
  - new user creation: after creating new user, specify programmatic access (get an access Key ID and secret access keys) and/or Console access (password) 
  - Secret access key only shown once and together with access key ID - gives you programmatic access to AWS CLI by allowing you to "login" or authc
- "Owner" refers to the identity and email address used to create the AWS acc
- also IAM Certificate store exists for you to upload certs

## AWS Identities used for Authentication ##
- **AWS account root user**
  - created with AWS ACC - has full access to all services/resources within acc
  - sign in with email
  - best practice is to create an IAM user and then let that one do all tasks
- **IAM User**
  - by default has no access to any AWS services
  - must manually grant perms
  - can generate access keys that allow you to authc via CLI and perform actions as the IAM user
    - not most secure as it leaves your creds on the machine you authc
  - can be grouped under IAM group for ease of bulk administration
- **IAM Role**
  - "hat" you can place on anyone who needs it
    - does not require password or access keys
    - provides you temp security credentials for your role session
  - Commonly grant access for these identities:
    - *Federated User Access*   
      - SAML (AWS SSO)  - can work with AD FS 
      - Web ID Federation - Amazon Cognito (mobile apps login with facebook or google)
    - *AWS Service Access*
      - service can assume the role to perform actions on your behalf - (ec2 can list s3 bucket or write to bucket)
    - *Apps running on EC2*
      - make an instance profile that contains the role and enables programs running on EC2 to get temporary creds

# S3 - Simple Storage Service

- It is Object-based Storage i.e allows you to upload files into secure, highly durable, scalable obj storage 
  - spread across multiple devices and facilities and you have virtually unlimited storage in "buckets" 
  - has security ability to log access data 
    - can configure MFA delete to prevent accidental deletion of data 
  - key-value storage
- Files can be 0B - 5TB 
  - HTTP 200 when file uploads successfully 
  - not a traditional fs suitable for OS or DB installs
- buckets have a universal namespace - needs to be unique for a unique web address 
- buckets created are private by default, need to be manually opened to the public

https://s3.regioname.amazonaws.com/bucketname
- Read/Write consistency
  - Read after write consistency for PUTS of new objects (if you upload an object - can see it immediately)
  - Eventual Consistency for Overwrite PUTS and DELETES (can take sometime for new version propagate throughout AWS)
    - A process writes a new object to Amazon S3 and immediately lists keys within its bucket. Until the change is fully propagated, the object might not appear in the list.
    - A process replaces an existing object and immediately attempts to read it. Until the change is fully propagated, Amazon S3 might return the prior data.
    - A process deletes an existing object and immediately attempts to read it. Until the deletion is fully propagated, Amazon S3 might return the deleted data.
    - A process deletes an existing object and immediately lists keys within its bucket. Until the deletion is fully propagated, Amazon S3 might list the deleted object.
  - when you pull down items in parallel, it can create these kinds of discrepancies

- **Components of S3**
  - Key: is simply name of the object
  - Value: is the data obj itself
  - Version ID
  - Metadata: is data about data you are storing
  - Subresources (Access Control Lists, policies and Torrent)

## Tiered Storage & Billing ##

- *Tiered Storage* (ordered most expensive to least)
  - **S3 Standard** -- (99.99% availability) 99.999999999% (11 9's) durability and is designed to sustain loss of 2 facilities (Availability Zone) concurrently
    - designed and best used for items that can't be lost and things you need immediately and access often
  - **S3 IA** -- (99.9% availability) - Infrequently Accessed but will be ( charged a retrieval fee ) 
    - designed for items that can't be lost and things you need immediately - but maybe don't access as often
  - **S3 Intelligent Tiering** -- uses ML to move S3 objects to different tiering levels for cost optimization 
    - designed to optimize costs by automatically moving data to the most cost effective tiers with out performance impact or operatioal overhead
  - **S3 One Zone IA (RRS)** -- (99.5% availability) Infrequently Accessed but only one AZ 
    - designed for items you can lose or recreate but still want immediate access to
  - **Glacier** -- Data Archival and cheapest Expedited, Standard or Bulk ( charged retrieval fee )
    - Data Retrievals:
      - Expedited few mins - quickly access data < 250MB when theres an urgent request
      - Standard 3-5 hrs 
      - Bulk > 5hrs
    - ALL data retrievals are subject to *provisioned capacity*: cap on # of expedited retrievals and throughput
      - default is 3 expedited retrievals every 5 min with 150MB/s throughput
      - used when you NEED data pronto and cannot allow an expedited request to be denied
    - secure, reliable, low cost storage class for data archival and retrieval time can be configured from mins to hours
    - designed for highly durable items that you don't need to access immediately 
  - **Glacier Deep Archive** -- low cost, but retrieval can take up to 12 hrs

S3 has an availability of 99.99%, S3-IA has an availability of 99.9% while S3-1Zone-IA only has 99.5%.
S3 durability 99.999999999% for ALL except S3 RRS

- **Billing** 
  - No Retrieval fee for Standard, but fee exists for all other tiers 
  - Charged for:
    - Storage used in GB
    - \# of requests 
    - Storage management pricing (tier you choose)
    - data transfer pricing (data retrieved & moved region to region)
    - Transfer acceleration (if you use cloudfront edge locations)
    - cross region replication

S3 Pricing:
  -- Storage Used
  -- Network Data In
  -- Network Data out (from EC2, S3...to Internet)
  -- Data Transfer out from Region to other region
  -- Data Request (PUT GET DELETE Requests) 
  -- Data Retrieval
- *NOTE* you do NOT get charged when data is transferred from s3 to:
  - ec2 inst in the same region as the bucket
  - cloudfront

Individual Objects in S3 dont inherit the bucket tags

Check Transfer Acceleration and Edge Locations caching  

*** Read S3 FAQs before exam

**S3 Security & Encryption**
- By default, all buckets created as Private, including objects inside
  - resource owner needs to manually open the bucket changing the bucket policies, and then set obj itself to public
- Control access to Bucket 
  - Bucket Policies - control bucket itself -> can be used for cross account access or copying
  - ACLs that can be granular and tied to indv objects and users (not applicable for granting public access)
- Can configure *server access logging* to track detailed bucket and obj level operations:
  - each request to S3 bucket including requester, bucket name, request time, request action, referrer, turnaround time and error code info
  - CloudTrail does not capture referrer and turn-around time, so you need server access logging to get more obj level operations
- Encryption in Transit (SSL/TLS)
- Encryption at Rest --> Server Side Encryption (SSE) or Client
    - SSE S3 (x-amz-server-side-encryption)
    - SSE AWS Key Management Service (KMS)
    - SSE Client provided keys (C)
    - Client library such as Amazon S3 Encryption client
  - Client Encryption --> encrypt the file and upload it 
    - This is how client-side encryption using client-side master key works:

When uploading an object - You provide a client-side master key to the Amazon S3 encryption client. The client uses the master key only to encrypt the data encryption key that it generates randomly. The process works like this:

1. The Amazon S3 encryption client generates a one-time-use symmetric key (also known as a data encryption key or data key) locally. It uses the data key to encrypt the data of a single Amazon S3 object. The client generates a separate data key for each object.

2. The client encrypts the data encryption key using the master key that you provide. The client uploads the encrypted data key and its material description as part of the object metadata. The client uses the material description to determine which client-side master key to use for decryption.

3. The client uploads the encrypted data to Amazon S3 and saves the encrypted data key as object metadata (x-amz-meta-x-amz-key) in Amazon S3.

When downloading an object - The client downloads the encrypted object from Amazon S3. Using the material description from the object's metadata, the client determines which master key to use to decrypt the data key. The client uses that master key to decrypt the data key and then uses the data key to decrypt the object.


## Versioning and Locks ##
- Stores all versions of an obj (includes writes and deletes)
  - excellent backup tool
  - tied to the bucket itself
- Versioning once enabled cant be disabled only suspended
- Versioning can use Multi Factor Authentication (MFA) for additional layer of security for deletion

- Versioning in action:
  - when you upload a new version of a file - it resets public access (need to re-open it to the public [perhaps it reverts to default?])
    - old versions retain original permissions and properties
  - If you are versioning - you pay for total storage of all versions
  - if you delete an object with versioning - a delete marker hides the object unless you show versions
    - to "undo" a delete, you delete the delete marker
    - you can also manually delete individual versions - this allows you to delete objects entirely 

Transition to S3 IA min 30 days after object creation
                 Glacier 60 days after object creation

**Life Cycle Management**   
- Used to automate moving of obj btw storage tiers 
  - Transition actions define transitions to another storage class
  - when storing in S3-IA and S3-One-Zone IA, it must be stored for at least 30 days before transfering to another class
  - Expiration actions specify for S3 to delete obj on your behalf
- can be used be used in conjunction with versioning 


**S3 Object Lock**
- WORM: Write Once, Read Many
  - prevents object from getting deleted and maintain integrity of the object (ensure no one can modify it)
  - can be applied to an obj or the bucket itself 
- Two modes: Governance and Compliance mode
  - Governance Mode:
    - Cannot overwrite or delete obj version or alter lock settings unless you have certain/special permissions
  - Compliance Mode
    - protected obj cannot be overwritten or deleted by anyone (not even root user) for the retention period 
    - Retention period: protects obj for a certain amount of time, after expiration you can modify/delete
- Legal Hold: prevents obj version from being overwritten or deleted (no retention period, so it lasts forever)

**Glacier Vault Lock** - NOT the same as S3 Obj locks - applies to Glacier Vault 
- Deploy and enforce compliance controls for individual glacier vaults 
- once locked, policies cannot be altered or changed

## S3 Performance ##
- Prefix: stuff between bucket name and object name (pathway or "folder structure")
- S3 has low latency and high throughput but it does have its limits
  - 3500 PUT/COPY/POST/DEL per prefix and 5500 GET/HEAD per prefix
  - this means to increase performance - SPREAD FILES ACROSS MORE PREFIXES 
- S3 limitations with KMS
  - It will decrypt file before download and encrypt before storage 
  - KMS service has quotas of requests per second it will allow - each encrypt/decrypt counts toward quota
    - there is NO way to increase the KMS quota 

- *S3 Transfer Acceleration* -
  - uses CloudFront Edge Network to accelerate your uploads to S3	 
  - Instead of using S3 URL, you can use a distinct URL to upload directly to Edge Location which will transfer then to S3

**Multi-part Uploads & Byte fetches**
- Recommended for files 100 MB + and required for files 5 GB + 
  - (largest PUT of obj in S3 is 5GB)
  - parallel upload by breaking file into parts and uploading in parallel 
  - s3 concatnates parts in ascending order based on part number 
  - AWS only combines parts after all parts uploaded and successful - if some part doesn't complete - it hangs for a certain TTL (7 days)
- Byte Range Fetches
  - Parallel downloads by specifying byte ranges 
  - speed up downloads or download parts of a file (header) 

## S3 Select & Glacier Select ## 
- Retrieve data using SQL Expressions from obj
- You can perform S3 Select to query only the necessary data inside the CSV files based on the bucket's name and the object's key.
    - you can NOT use obj metadata or obj tags to query with S3 select 
  - use sql to filter data you need from a particular object - subsetting rows/cols
  - save time on execution and money on data transfer 
    - Assume you have a csv in a zip file... default approach is to download the zip, unzip and query the csv
    - with S3 select you can use SQL to download data you need 
    - works with CSV, JSON, Parquet, JSON arrays, and bzip2 compression for csv and JSON
- Glacier Select allows you to run SQL querires on glacier directly

S3 Select is an Amazon S3 feature that makes it easy to retrieve specific data from the contents of an object using 
simple SQL expressions without having to retrieve the entire object. Amazon Redshift Spectrum
 is a feature of Amazon Redshift that enables you to run queries against exabytes of unstructured data 
 in Amazon S3 with no loading or ETL required.


- **Cross Region Replication** - requires bucket versioning on both source and target bucket
- Versioning must be enabled on source and destination buckets for cross-region replication
- Delete Markers are not replicated to avoid accidental deletion on cross region replicated bucket 
- Can replicate based on bucket, or tags/prefix... 
- Once a replication bucket is created, it is created as empty. 
  - it will only take on new objects or subsequent obj from the source
  - if you delete an obj on the source, the replicated bucket does not replicate the delete marker or the deleted version (to prevent from accidental deletion)

## Website Hosting ## 
- S3 Bucket name should be the same as the domain name, and you need a registered domain name 
- to route traffic to website hosted in S3 bucket: 
  - s3 bucket is configured to host a static website
  - bucket must have same name as domain or subdomain (example.com is name of your bucket)
  - registered domain name - either through Route 53 or other registrar
  - Route 53 as DNS service for domain 

### Quiz ###

Check Amazon S3 offers encryption services for which types of data ?
Amazon offers encryption services for data at flight and data at rest.

Bucket Policy necessity ?
Users need a bucket policy to grant or deny accounts to read and upload files in your bucket. 

Amazon S3 offers three pricing options. 
Storage (per GB per month), data transfer in or out (per GB per month), and requests (per x thousand requests per month).
 
AWS Import/Export accelerates moving large amounts of data into and out of AWS using portable storage devices. 
AWS transfers your data directly onto and off of storage devices by using Amazon's internal network and avoiding the Internet.

Object storage systems are typically more efficient because they reduce the overhead of managing file metadata by storing the metadata with the object.
 This means object storage can be scaled out almost endlessly by adding nodes.
 
Amazon S3 provides a simple, standards-based REST web services interface that is designed to work with any Internet-development toolkit. 
The operations are intentionally made simple to make it easy to add new distribution protocols and functional layers. 

*** Read S3 FAQs for Exam	
https://aws.amazon.com/s3/faqs/	


The volume of storage billed in a month is based on the average storage used throughout the month.
This includes all object data and metadata stored in buckets that you created under your AWS account.
We measure your storage usage in “TimedStorage-ByteHrs,” which are added up at the end of the month to generate your monthly charges.

**S3 Bucket Names**
- Virtual Style - new standard
    ```https://<bucket-name> . s3 . <aws-region> . amazonaws.com/obj-name```
- Path Style - being deprecated
    ```https:// s3 . <aws-region> . amazonaws.com/<bucket-name>/obj-name```
- Legacy Global - US East 1 default
    ```http:// <bucket-name> . s3 . amazonaws.com/obj-name```  
https://docs.aws.amazon.com/AmazonS3/latest/dev/UsingBucket.html 



# AWS Orgs and Consolidated Billing #

- AWS Orgs: consolidate multiple AWS acc into an org you create and centrally manage 
  - Housed under 1 root account, you create OU or Org Units with individual AWS Acc underneath 
  - This hierarchical structure allows you to create policies that control access to AWS Services "Service Control Policies" 
    - For example - block EC2 access to the Finance team... 
- Paying Account - and linked accounts are independent 
  - can be consolidated into 1 bill - and track across individual accounts to see costs 
  - can aggregate across all accounts to create single bill - eligible for volume pricing discounts 
- Consolidated Billing is a feature of AWS Organisations. Once enabled and configured, you will receive a bill containing the costs and charges for all of the AWS accounts within the Organisation. Although each of the individual AWS accounts are combined into a single bill, they can still be tracked individually and the cost data can be downloaded in a separate file. Using Consolidated Billing may ultimately reduce the amount you pay, as you may qualify for Volume Discounts. There is no charge for using Consolidated Billing.

**Root account Security**  
- Always enable MFA on the root account
- Strong Complex password 
- Paying account should be for billing only - don't deploy resources as the paying account 
- Enable/disable AWS services using service control policies on OU or AWS Acc



# CloudFront #

- Part of AWS's CDN -- Content Delivery Network
  - system of distr servers delivering web content to users based on geo location
  - for static and dynamic content - over HTTP use CloudFront
- Edge Location: It is the location where content is cached. This is separate to an region/AZ
- Origin: This is origin of all the files that CDN will distribute. This can be either S3 bucket, EC2, Elastic Load Balancer or Route 53
- Amazon Cloudfront can be used to deliver your entire website including static, dynamic, interactive and streaming content using a global network of edge locations
  - TWO TYPES: 
  - Web Distribution -- used for Websites
  - RTMP which does media streaming
- Distrbution is name given to collection of Edge Locations
- Edge Locations are not read only, you can also write to them to "pre-cache" content
  - obj are cached for specified TTL or based on a policy 
  - cached content can be cleaned/invalidated but there is a $ charge (in case you need to update a file)
- You can have multiple origins with in same distribution
- can serve static and dynamic content

## Securing access to certain files ##
- You can restrict access to Cloudfront using Signed URLs or Signed Cookies (like Netflix which uses pay per view)
- CloudFront doesn't expose Amazon S3 URLs, but your users might have those URLs if your application serves any files directly from Amazon S3 or if anyone gives out direct links to specific files in Amazon S3. 
- **Signed URL** work for a single file 
  - does not work for RTMP
  - for restricting accesss to indv files (install download)
  - OR client is using an HTTP client that doesn't support cookies
- **Cookies** work for multiple files
  - provide multiple restricted files - or files for a video... 
- both can work with policies which can further define url expiration, IP ranges and trusted signers...
- Cloudfront URL can access EC2 and use the policies to filter by date, expiry, IP... 
  - considered more secure as the user is going through cloudfront to get the obj or origin file, not directly accessing origin
- S3 URL has a TTL and is a pre-signed url that grants user access to a file 
- **Origin Access Identity**
  - If you use CloudFront signed URLs or signed cookies to restrict access to files in your Amazon S3 bucket, you probably also want to prevent users from accessing your Amazon S3 files by using Amazon S3 URLs. 
  - If users access your files directly in Amazon S3, they bypass the controls provided by CloudFront signed URLs or signed cookies. This includes control over the date and time that a user can no longer access your content, and control over which IP addresses can be used to access content. In addition, if users access files both through CloudFront and directly by using Amazon S3 URLs, CloudFront access logs are less useful because they're incomplete.
  - to solve this problem - create OAI -- To ensure that your users access your s3 files using only CloudFront URLs, regardless of whether the URLs are signed
  - Create an origin access identity, which is a special CloudFront user, and associate the origin access identity with your distribution. 
  - Change the permissions either on your Amazon S3 bucket or on the files in your bucket so that only the origin access identity has read permission - deny direct URL access 
    - OAI acts as a proxy for your users, only OAI has access, users cannot access directly 
  - cloudfront can have an origin failover policy 
    - basically a DR for the origin, if default origin A is down, fetch data from backup origin B


- There are three options in the question which can be used to secure access to files stored in S3 and therefore can be considered correct. Signed URLs and Signed Cookies are different ways to ensure that users attempting access to files in an S3 bucket can be authorised. One method generates URLs and the other generates special cookies but they both require the creation of an application and policy to generate and control these items. 
- An Origin Access Identity on the other hand, is a virtual user identity that is used to give the CloudFront distribution permission to fetch a private object from an S3 bucket.
- Public S3 buckets should never be used unless you are using the bucket to host a public website and therefore this is an incorrect option.

- if you want to allow multiple domains to serve SSL traffic over the same IP address: SNI
  - SNI (service name indication) on CloudFront or App LB
  - allows multiple domains to serve SSL traffic over the same IP address  
    - "all you need to do" is bind multiple certificates to the same secure listener on your App LB - and it will choose optimal certificate for each client (free!)
  - cloudfront will deliever content from each edge location and offer same IP TLS/SSL
  - you can apply your cert to cloudfront, but it must come from AWS cert manager or IAM cert store


# AWS Snowball, Snowball Edge & Snowmobile #
- Snowball -- import to S3 or export from S3
- Snowball is a physical petabyte-scale data transport that uses secure appliance to transfer data in and out of AWS rather than over the publicnetwork
- Snowball comes in either 50TB or 80TB size
  - AWS guarantees tamper resistant enclosures, 256 bit encryption, trusted platform module, full chain of custody and wipe device after transfer 
- Snowball Edge is 100TB data transfer device with on-board storage and compute capabilities.  And also you can run Lamba functions there
  - to support local workloads in remote or offline locations (Airlines use this capability)
  - can even be clustered 
- Snowmobile - 100PB and uses 45Ft long Truck to transfer in and out of AWS
  - basically a truckload of snowballs
- should use snowball if your speed is < 100 MB/s for 5 TB +   or 1000 MB/s for 60 TB +  



# Storage Gateway #
- Connects on-premise software appliance with cloud based storage to provide seamless and secure integration btw organization's on-premise IT and AWS's storage infrastructure 
- **Expand your on-premise storage capacity with AWS cloud & continue to access data on-prem**  
  - *NOT like data migration services to MOVE data into AWS* 
  - physical or virtual appliance to cache S3 locally 
  - Used for low-latency access to data, with AWS for durable storage
    - As it gets used, sends changes back to AWS rather than whole files 
- 3 kinds:
  - **File Gateway** (NFS and SMB)-- Flat files stored directly on S3 
    - Accessed locally through NFS mount -> file gateway as a file system mount on S3 
    - SSSD stored in S3 metadata (ownership, perms, timestamps...)
    - S3 also comes with further benefits of versioning, lifecycle management, cross region replication...
    - low latency with main storage in S3
  - **Volume Gateway** (iSCSI) --  copy of hard disk drives on S3
    - will present apps with disk volumes using iSCSI block protocol 
    - *Stored Volumes* | *Gateway-Stored* -- Entire dataset is stored on site and asynchronously backed to EBS or S3 (1GB - 16TB)  
      - Data written to volumes asynch copied to AWS 
      - can write to S3 or EBS snapshots -> incremental backups can capture changes and be compressed for cost savings  
      - low latency access with backup in AWS
	- *Cached Volumes* | *Gateway-Cached* -- Entire dataset resides on S3 and the most frequently accessed data is stored on-site (1GB - 32TB)
      - S3 is primary storage, rather than asynch backup - AWS will cache most frequently accessed content "locally" on premise
      - low latency for freq accessed items, and main storage in AWS
  - **Tape Gateway** -- *Gateway-VTL* Virtual Tape Library  
    - useful for moving your backups to cloud and very cost effective and durable 
      - can directly link to Glacier or Glacier Deep Archive
    - slow retrieval times

# AWS Data Synch # 
- move large amounts of data and large sets of data into AWS for backups, migration or transfer data to AWS etc quickly
  - if question asks to MOVE data to the cloud, and they will NOT access it from on-prem
- agent deployed on a server (on-premise or in AWS EFS) and copies NAS file system data to AWS target of S3, EFS, or AWS fs for Windows
- used with NFS/SMB fs, to AWS target: S3 (can dump directly into Glacier, Glacier Deep Archive), EFS, or FSx for Windows
- Can schedule replications hourly/daily/weekly 

# Athena vs Macie # 
- **Athena** serverless interactive query service to analyze and query data located in S3 using std SQL
  - pay per query and per TB scanned 
  - No ETL - executes in parallel, works directly with certain kinds of data in S3
    - CSV, JSON, Apache ORC, Apache Parquet, & Avro
  - Used for scanning Access logs, ELB logs...
  - generating reports on data - AWS cost reports in S3, clickstream data... 
  - often used with big data and interactive. no need to load data
- **Macie** Security service that uses NLP & ML to discover, classify and protect sensitive data in S3
  - goes around scanning your data and encrypting PII and preventing ID theft
  - can even analyze cloudtrail logs for sus activity




# EC2 - Elastic Compute Cloud #
- EC2 is a web service that provides resizable compute capacity in the cloud
  - reduces time to acquire and boot compute - allowing for scalability 
- Pay as you go, pay for what you use, pay less as you use more, pay less when you reserve 
- Runs on Nitro & Xen Hypervisors
- Default Limits for EC2 creation: 
  - limit of 20 RIs per region - can request to be raised
  - You are limited to running On-Demand Instances per your vCPU-based On-Demand Instance limit, 
  - requesting Spot Instances per your dynamic Spot limit per region. 

## Pricing Model ##
- **On Demand Pricing** -
  - Users that want low cost and doesnt want to pay upfront fees and long term commitments
  - For Applications being developed and tested for the first time
	- Spiky and unpredictable workloads

- **Reserved Pricing** - get discounts for length of term
  - For Applications with steady state or predictable usage
  - Applications that require reserved capacity
  - CANNOT be moved to a new region
  - Users able to pay upfront costs to reduce their total computing costs
    - Standard Reserved instances up to ~ 75% off
    - Convertible Reserved instances - allows you to change instance type
    - Scheduled Reserved instances - launch for specified time window  
  - *Termination policies*
    - when RI expires, any inst covered by the RI are billed at on-demand rates - so you want to terminate RI to avoid this cost

- **Spot Pricing** --  can bid the price
  - Apps that have flexible start/stop time
  - heavy urgent compute needs 
  - can grab extremely low compute costs
  - if spot instance terminated by AWS - you don't pay for partial hour BUT if you terminate, you pay for partial hour

- **Dedicated Hosts** - reserve physical server
  - often for regulator req that may not support multi tenant virtualization
  - Great for licensing which doesnt support multi-tenancy or cloud deployments  (like oracle)
	- Can be on-demand (hourly)
  - compared to "shared" there are two options: Dedicated inst and Dedicated host (additional visiblity into physical server - can run hardware-bound licensed software/address regulations)
  - Can set a VPC to dedicated hosting - to change to default, can use CLI/APIs or SDK to change VPC setting and type - it only changes future inst (not current ones)

- **Startup/Termination Fees**
  - During "stopping" state:
    - You are billed when inst is preparing to hibernate
    - you are NOT billed when inst is goign to be stopped
  - During "terminated" state:
    - you are billed if you have active RI
  - During "pending" state:
    - you are NOT billed 

## Spot Instances & Fleets ##
- **SPOT INSTANCE** unused EC2 that can be terminated in 1-2 minutes 
  - used for stateless, fault-tolerant or flexible apps:
    - Big Data
    - Containerized Workloads 
    - CI/CD
    - HPC
    - Webservers
    - DEV/TEST
  - NOT good for: 
    - persistent workloads (stateful)
    - Databases (duh)
    - critical jobs

### How it Works ###
- you decide on a spot price and provision instance - so long as price is below your max bid
  - can pay for an additional spot block - allowing you to prevent the 2 min termination by 1-6 hours. 
- To fill a spot request:
  - Max price you are willing to pay
  - \# of instances
  - Launch specs 
  - Type [ one-time | persistent ]
    - one-time dies after price exceeds your bid 
    - persistent will recreate instances whenver price below bid 
  - valid dttm from -> to 
- **Termination**
  - when spot price > max bid
  - no capacity
  - EC2 can't meet your constraints
  - upon the interruption notice - your inst can be set to hibernate, stop or terminate

### SPOT FLEET ### 
- Combo of spot inst & (optional on-demand inst) to meet target capacity 
- Used when you need a min # of servers -> AWS will try to match target capacity within requirements (only fulfilled if AWS has capacity)
- define a spot instance pool:
  - set of ec2 inst with same type, OS, AZ and network
- Your spot fleet will fulfill demand request based on your spot instance pools availability 
- Several balancing/launching algos: 
  - Capacity Optimized 
    - instances that can meet min req are pulled from the pool 
  - Lowest Price
    - instances with lowest price are pulled from appropriate pool
  - Diversified 
    - inst distributed across all pools 
  - Inst Pools to use Count
    - used in combo with lowest price to distr across specified pools 

** TIPS ** 
- Spot instances can save up to 90% 
- useful for non-critical non-persistent workloads
- can block spot inst termination with a spot block 
- use a spot fleet for spot instance collections 

### EC2 Types - FIGHTDRMCPXZAU ###
- F1 - field programmable - crazy complex AI/ML
- I3 - high speed storage
- G3 - Graphics for video encoding 
- H1 - High disk throughput
- T3 - "Tiny" for webservers and small DBs
- D2 - dense storage (file servers)
- R5 - "RAM" memory optimized 
- M5 - General purpose (app servers)
- C5 - "compute" optimized
- P3 - graphics cards - GPU
- X1 - Mega Memory optimized (HUGE RAM servers)
- Z1D - High compute and memory footprint 
- A1 - ARM based scalable web servers 
- U- - Ultra RAM with 6+ TB RAM per instance
 
## Accessing and Working with EC2 ## 
- to access EC2 you can use:
  - SSH client
  - ec2 instance connect (browser based ssh)
  - java ssh client from browser 
- can encrypt the root vol device 
- cloudwatch monitors every 5 min, can pay for 1 min updates 
- when you look at status check in AWS for your ec2 instance:
  - System check - ensures AWS working to reach instance 
  - Instance check - ensures instance is up and network avail   


## Security Groups ##
- basically fancy firewall specs that can get assigned to EC2 inst
  - does NOT filter traffic, only controls ports open/closed
- Any time you change Security Group, it will be immediate effect
- All inbound traffic is blocked by default and you have to allow by defining rules
  - All outbound traffic is allowed
- ***Stateful*** service (Example: Allow http in automatically allowed http out)
- No way of blacklisting particular port or ip address 
  - Must use NACLs Network ACLs - Stateless service to block IP
  - NACLs can blacklist traffic, etc... Security groups cannot
- Can assign security groups to many EC2s and they can be applied in layers 

## EC2 Actions & Lifecycle ## 
- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-lifecycle.html 
- Instance Lifecycle: 
![Instance Lifecycle](/images/instance_lifecycle.png)
  - AMI will Launch -> "pending" (inst is preparing to enter a running state - after launch or starting a stopped inst)
  - After it is started it will be -> "running" (ready for use)
  - From there it can go into a stopped state during hibernation or stopping -> "stopping" -> "stopped"
  - It can also be rebooted "rebooting"
  - It can also be terminated "shutting-down" -> "terminated" (perm delete and cannot be restarted)

- variety of actions [start | stop | terminate | hibernate] 
- EC2 Stop ~ shutdown computer
  - when you stop, EBS persists with instnace even though it is "shutdown"
  - Public IP is released UNLESS you have elastic IP assigned
  - Private IP and ENI stays with the instance
  - *could* move to another host machine (as ec2 is a vm)
- EC2 Start ~ turn on computer
  - OS boots up; and user data script runs; apps start 
- EC2 Terminate ~ wiping computer 
  - Removes instance and (by default) the root vol 
  - Termination protection is turned off by default - must manually turn on 
  - EBS backed instance will delete root volume by default, 
    - additional volumes are not deleted by default 
  - EBS root vol can be encrypted and so can additional volumes 
- EC2 Hibernate ~ hibernate/sleep computer 
  - saves OS RAM to EBS root vol with a *suspend to disk* 
  - now when you reboot -> RAM content is loaded: there is no need to start services all over again 
  - DOES NOT restart instance 
  - What it does:
    - EBS root volume is restored
    - RAM contents are reloaded 
    - processes running are resumed 
    - data vols reattached 
    - instance retains original InstID 
- Hiberation useful for:
  - Long running processes
  - Things that take a long time to start 
- Hibernation requires:
  - root vol storage large enough to hold RAM contents
  - root vol encryption 
  - Inst RAM < 150 GB
  - Hibernation period < 60 days 


# EBS - Elastic Block Store #

- Amazon provides persistent block storage volumes for use with Amazon EC2 Instances in Amazon Cloud
  - Volumes exist on EBS and think of them as virtual hard disks 
  - *automatically deployed in same AZ your instance is running*
- *EBS is automatically replicated in Availability Zone* to protect you from component failure, offering high availability and durability 
  - meaning, if you want to MOVE inst to another AZ (or region), you will need to snapshot AMI (see below)
  - once a volume is created, can attach to any inst within SAME AZ
  - EBS can only be attached to 1 ec2 inst at a time 
  - persists independently from life of EC2
  - support live config changes in prod (can modify volume type, size, IOPS capacity...) without service interruptions
  - encrypted using AES-256
  - can handle up to 16TB on a single volume or add new ones for additional storage

  > [EBS Volume Types](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-volume-types.html)   

| EBS Volume Type                                      |  Max IOPS / Volume  |  API Names |  Order of Cost  |
|------------------------------------------------------|---------------------|------------|-----------------|
| General Purpose (SSD)                                |    16000            |  gp2 / gp3 |  1  ($$$$$)  |
| Provisioned IOPS (SSD)                               |   64000 (on nitro inst)            |  io1 / io2 |  2  |
| Throughput optimized Hard Disk Drive (HDD)           |     500             |    st1     |  3  |
| Cold Hard Disk Drive (HDD)                           |     250             |    sc1     |  4 - more expensive than s3 |
| Magnetic                                             |    40-200           | standard   |  5  ($)  |

- TIP: EBS Volumes need to be in same AZ as your EC2 Instances to avoid latency  
**SSD vs HDD**
- SSD -- small, random operations  -- IOPS --  --Large Databases MongoDB, Oracle, MS SQL -- best for transactional workloads
- HDD -- large sequential  -- throughput -- Infrequently Accessed -- BigData, Datawarehouse, Log Processing --- large streaming workloads
  - if you are using infrequently accessed data - can put on Cold HDD: sc1

## Working with EBS Volumes - Snapshots & AMIs ##
- AMIs (Amazon Machin Image) can be created from Snapshots 
  - AMI is an EBS volume + OS launch info - so that when you launch AMI, you just pick instance, and networking info
  - Snapshots are stored on S3 and think of them as photograph of disks 
  - Snapshots are point in time copies of volumes  - backup to s3
  - they are incremental meaning that only blocks that have changed since your last snapshot that was moved to S3
  - First snapshot may take some time
  - AMI can be created based on Region, OS, Bitness (32 or 64 bit), Root Device Type (EBS or Instance Store) Launch permissions
- Best practice to take a snapshot when instance is down but you can still take snapshot when instance is running
- Can also modify volume size and type when instance is on - if you change size, need to repartition drive so OS can see space 

- AMI can be used to launch instance in another Availability Zone
- **Data Lifecycle Manager**
  - automate creation, retention and deletion of snapshots taken to back up EBS volumes
  - enforce regular backup schedule 
  - retain backups as required by audit/compliance
  - reduce storage by deleting outdated backups
  - easier than creating CLI job to take snapshots of jobs

### This is how you can copy AMI from one region to another region ###

1. Create a snapshot of your EBS volume 
2. Then create an AMI with your snapshot (can copy AMI to different region)
3. Use AMI to launch instance in another AZ 

```Important to note: AWS does not copy launch permissions, user-defined tags, or S3 bucket perms, from source to target AMI in new region```
- it does carry over user-data

### EBS vs Instance Store ###
- AMI can be launched based on: 
  - Region, OS, 32/64 bit, Launch permissions
  - AND the *Root Device Storage* - which can be: 
    - Instance store (ephemeral)
      - Ephemeral Storage and root volumes with Instance Store AMIs are based on template stored in S3
    - EBS Backed Volumes
      - root device launched from AMI is an EBS volume created from an EBS Snapshot

- Instance Store Volumes behave differently:
  - cant be stopped and if your underlying host/hypervisor fails you will lose your data
    - if inst is stopped or terminated - you will lose your data
    - instance can be rebooted and data will persist
  - often has restrictions on EC2 type
  - your root volume is the instance store - meaning it cannot be changed
    - does not affect your ability to attach other instance stores during launch or EBS volumes AFTER launch

- EBS backed instances can be stopped and you will not lose data if instance is stopped
- You can reboot both and you will not loose data. You will lose data with Instance Store only if the underlying host fails
- By default root volumes will be deleted with EBS and Instance Store and however with EBS you can tell AWS to keep root volume  


## Encrypting Volumes & Snapshots ##
- can create a root vol that is encrypted using KMS key or default encryption

- however - you may want to encrypt after its been created:
  1. Create a snapshot of root volume which is unecrypted
  2. Create a copy of the snapshot and choose encrypt option
  3. Create AMI using encrypted snapshot
  4. Launch instance using created AMI  

- snapshots and restored vols of encrypted vols are encrypted by default 
- sharing of snapshots only works if they are unencrpyted 
  - can be shared with other aws acc but they must be made public and unencrypted
- Alternatives to encrypted volumes:
  - 3rd party volume encryption tools 
  - native OS encryption tools (windows bitlocker)
  - encrypt data within app and then store on EBS 




# Elastic Network - ENI vs ENA vs EFA # 

### ENI - Elastic Network Interface (basic networking) ###
- a virtual NIC 
- private IPv4 address
- can get mapped to 1 elastic IP
- provides 1 + private IP
- can be assigned 1 + security groups
- only 1 public IP 
- MAC address 
- Source/dest check flags 

- used for: 
  - basic networking for cheap
  - create a management network
  - low-budget HA solutions 
  - dual-homed instances with workloads on distinct subnets (DEV vs PROD)

### ENA - Elastic Network Adapter (faster network) ###
- single root I/O virtualization to provide HP network capabilities on certain EC2 types 
  - basically tie a virtual NIC to 1 physical machine
- created for a faster/better network than ENI (reliable high throughput network) 
- more for network to/from apps running on machine
- Speeds up network by 
  - higher bandwidth
  - lower latency 
  - higher packets per second 
- ENA supports speeds up to 100 Gbps 

### EFA - Elastic Fabric Adapter (HPC or ML) ###
- device you can attach to EC2 to accelerate ML & HP compute
- consistent latency and higher throughput than TCP in a typical cloud 
- uses OS-bypass to communicate directly with EFA device skipping OS translation (linux only)
  - meaning it is very fast to get data to the CPU -> network



# CloudWatch & CloudTrail & AWS CLI & EC2 Roles #

## PerfMon vs Audit Trail ##
- **CloudWatch** is used for monitoring performance (cpu, network, storage...) 
  - can monitor most of AWS and its applications that run on AWS
    - compute (EC2), autoscaling, ELB, Route 53, EBS Vol, Storage Gateway, Cloudfront...
    - CPU, Network, disk r/w, status check..
    - CAN NOT monitor: mem util, disk swap, page file, log collection, disk space util... - need to set up a custom metric in scripts or install a CloudWatch Agent
  - has dashboards to view monitoring data 
  - alarms you can set yourself for billing, etc
  - events/triggers to take under certain circumstances (scale Up or down)
- CloudWatch with EC2 watches every 5mins... you can do up to 1min (detailed monitoring) which will be extra charge
  - Does NOT track things inside EC2 such as Memory
  - can track things "outside" ec2 like cpu usage, disk r/w [operations/bytes], network in/out...
  - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/viewing_metrics_with_cloudwatch.html
- Cloudwatch Custom Metric -
  - create a shell script on EC2 instance and run it using cron and then select the custom metric in cloudwatch by selecting the instance
- **Cloudwatch Events**
  - can trigger AWS resources do something or has changed when certain events occur - S3 upload or delete
  - can feed Amazon ECS directly (no need for lambda intermediary) -> create tasks or delete tasks
    - NOT useful for health checks
- **Cloudwatch Alarms**
  - set on a cloudwatch metric and will trigger
  - can do stuff like read cloudwatch logs for key words that create a custom metric -> then set up a cloudwatch alarm to trigger ec2 restart... 
- **Resource Group** can create resource groups containting sets of resources and view in a single dashboard - cloudwatch is one resource at a time
- **CloudWatch Logs Insights** interactively search and analyze log data in cloudwatch logs 
  - perform queries to respond to issues...

## CloudTrail ##
- **CloudTrail** "Audit Trail" is all about Auditing like peoples accessing S3 buckets, people launching EC2 instances, API Calls....(CCTV Analogy) 
  - who is provisioning S3 or EC2...as an example
  - tracking AWS console activity/configs (automated or manual) monitors API calls into AWS Platform -> compliance req & security
  - when working with global services:
    - need to add option --include-global-service-events to monitor WAF, IAM, CloudFront, Route 53...
  - puts all logs in S3 by default 

### Features ###
- Event history allows you to view, search, and download the past 90 days of supported activity in your AWS account. 
- can create a cloudtrail trail to further archive, analyze and respond to changes in AWS resources 
  - trail can deliver these events to an S3 bucket you specify

#### AWS Config ####
- service that enables you to assess, audit and eval configs of AWS resources
- tracks setup and config of your resources and can compare to compliance of your configs
- you set up a AWS Config rule to enforce ideal config in AWS acc
  - can check if there are any configs that violate this rule 
  - can show results in a dashboard 

- **TIPS** look for audit configs & setup vs audit changes or activity (CloudTrail)
- Look for phrases that imply identifying out of compliance settings or enforcing them "publicly accessible S3 buckets should be Identified..." (aws config)

## AWS CLI ## 
- using aws CLI or aws linux instances, can interact with AWS console through API calls 
  - without role assignment, need to use Access Key and Secret Access Key provided during user creation 
  - saved by default in $HOME/.aws/credentials file 
    - ! can be a major security risk as it is a raw unencrypted text allowing anyone to become that persona
  - once you log in "authc" with your keys - you inherit the rights associated with that persona  
-  Bootstrap scripts/ Bash Scripts/ User data...
  - used to automate deployments and run simple start/stop updates

**Roles - the secure way**
- Roles are the secure way to grant access rather than passing/storing access key creds
- can be centrally managed (universal - not regional) 
- can be managed via CLI or console 
- when you login as ec2-user you inherit the role

**EC2 Metadata**
- curl http://169.254.169.254/latest/meta-data/local-ipv4
- curl http://169.254.169.254/latest/meta-data/public-ipv4
- curl http://169.254.169.254/latest/user-data/
- curl http://169.254.169.254/latest/meta-data/
- can also get public key, instID and public IP



# EFS Elastic File System # 
- File Storage Service for Amazon EC2 instances (NFS for the AWS cloud!)
  - Multiple concurrent access to data from many ec2s...
  - ***Simple, scalable, elastic file storage and high throughput - POSIX compliant
  - Supports NFSv4 Protocol
  - provides file system access semantics (strong consistency and file locking)
  - Must be in the same security groups as your EC2 to be mounted
- EBS (with exception of io1/io2) is 1:1 to EC2 inst - EBS is more SAN and EFS is more NFS
- Pay for the storage you use and can scale up to petabytes
- Can support 1000s of NFS concurrent connections
- Read after write consistency
- Data is stored across multiple AZs in region
- General Purpose and Max IO to choose
- **EFS Lifecycle Policies**
  - max days for EFS lifecycle policy is 90 days -> cannot participate in lifecycle policies that retire data after 1 year (for example)

### FSx for Windows ### 
- built on Windows server for windows and windows Apps (SQL Server, Sharepoint, IIS Web Server)
  - AD Users, AC Lists 
  - Security policies
  - Distr F/S
  - namespace/replication
- runs windows server message block (SMB)

### FSx for Lustre ###
- Optimized for compute intensive workloads - HPC, ML, Data processing, electronic design auto 
- IMMENSE throughput - GB/s and low latency 
- AI/ML large datasets with stsrong throughput 
- Can store data directly on S3 



# EC2 Placement Groups #
- defining how EC2 inst get launched
  - only certain type of instances can be spun (Compute Optimized, GPU, Memory Optimized)
1. **Clustered Placement Group - group inst in 1 AZ** 
    - for low network latency and high network throughput with in same AZ
    - limited to certain inst
2. **Spread Placement Group  - place inst on distinct hw racks (can be spread to different AZ)**
    - smaller number of critical single instances (separating tiers of an app) 
    - maximum of 7 inst per AZ
3. **Partitioned Placement Group - spread PG with inst type specificity and allow multiple inst per "partition" AKA rack**
    - multiple instances in each parition (like partition 1, 2...)
    - multiple instances HDFS HBase Cassandra, Hadoop, and Kafka.
    - like setting up clustering for tiers of an app 

- Spread & Partitioned can span multiple AZ but must be in same region 
- PG names must be unique within AWS ACC
- Recommend smae kind of inst in clustered PG
- can't merge PG 
- can move stopped inst into PG via CLI (not console) 
- can create AMI and launch EC2 inst into placement group

# HPC on AWS # 
- gain full scalability and elastic compute capabilities 
- why? AWS has Data Transfer, Compute, Storage and Orch/Automation capabilities

1. **Data Transfer** 
    - Snowball / SnowMobile 
    - AWS Data Synch client - > client agent on premise can store data in AWS filesystems and s3
    - AWS Driect Connect - private VPN connection into AWS

2. **Compute** 
    - EC2, GPU/CPU, Spot Fleets, PG, 
    - Enhanced Networking 
      - ENA for High IO throughput
      -  EFA OS bypass for faster comm

3. **Storage**
    - inst attached
      - EBS 
      - Inst store
    - Networked Storage
      - S3
      - EFS
      - FSx for Lustre 

4. **Orchestration/Automation**
    - AWS Batch: run thousands of computing jobs 
      - can run multi-node || jobs to have a single job span multiple inst
      - scheduling/launching jobs based on reqs
    - AWS || Cluster: OSS cluster tool for deploying/managing HPC clusters 
      - uses text file to model & provision
      - Infra as code - to build VPC, cluster, types, etc 


# WAF - Web App Firewall # 

- L7 Firewall - Monitor & filter out evil HTTP requests to CloudFront, App LB, API Gateway  
  - controls access to your content 
- Can configure filters (web ACLs) that when violated provide 403 response 
  - Your webapp forwards request to WAF which checks whether its safe 
  - can restrict certain IPs to make changes or certain requests 
  - restrict params passed... 
    - IP addresses
    - HTTP headers
    - HTTP Body
    - URI Strings
    - SQL Injection 
    - XSS - cross site scripting
    - *can limit size of requests*
    - can limit rate of requests (help block DDoS with help of AWS Shield)
  - according to rule builder:
    - restricts based on country of origin
    - origin IP address
    - Request header, query parameter, multi query parameter
    - URI path, query string
    - Body, HTTP Method
  - allow all requests except ones specified
  - block all requests except onces specified 
  - count requests that match properties specified 
- ***Allows you to define conditions to protect against web attacks or block countries***
  - Can block country of origin
- History of WAF calls stored in CloudTrail
- Priced based on # of Web ACLs made, # of rules per web ACL and # of requests

- preferred for public webapps
- can be attached to CloudFront and block traffic there before it hits your VPC

## AWS Shield ##
- WAF is **DIFFERENT FROM** AWS Shield -- protects you by default against DDOS Attcks. 
  - default is L3 and L4 attacks like SYN/UDP floods, etc
- you can get advanced support as well
  - further protections traffic monitoring, notifications, attack mitigation, scales up..
- can do DDoS simulation testing or penetration testing with permission from Amazon TS


## Firewall Manager
- works with WAF to simplify WAF admin and maintenance - does NOT protect against DDoS


# Databases in AWS #

## Database overview ##
- Relational OLTP databases - RDS
  - OLTP: search for order # and get back name, address, date...
- Non-relational DB or NoSQL database - DynamoDB 
  - collections ~ table 
  - documents ~ rows 
  - key value pairs ~ fields 
  - fields are non standard - either they exist in a row or they don't 
- Data Warehousing OLAP - Redshift
  - Cognos, SQOL Server Reporting, SAP Netweaver, Jaspersoft, Oracle Hyperion... All require specific DB and infra
  - Used for business intelligence -> large, complex datasets running large queries for MGT reporting 
    - OLAP: Online Analytics Processing - example queries net profit for EMEA of digital radio product...
    - AWS's DW: Redshift 

- Elasticache 
  - Web service to deploy, operate and scale in-memory cache in the cloud 
    - faster than writing to disk in normal DB -> OPTION TO SPEED UP PERFORMANCE by taking load off database and can execute frequent queries 
  - can run 2 options - memcached (simple) or redis (advanced/durable)

- Remember that a relational database system does not scale well for the following reasons:
  - It normalizes data and stores it on multiple tables that require multiple queries to write to disk.
  - It generally incurs the performance costs of an ACID-compliant transaction system.
  - It uses expensive joins to reassemble required views of query results.

## RDS Details ##
- RDS OLTP Online Transaction Processing 
  - Multi AZs for DR -> AWS does auto failover to another AZ - synch write to AZ copy and no charge for data transfer
  - Read Replicas for Performance -> AWS allows you to point read requests to read replica - allow 5 copies max
  - Supports common DB:
    - MS SQL (licensing limits db per inst :: volume is iops - with 16TB max)
    - My SQL (no limits on db per inst)
    - Oracle (licensing limits db per inst)
    - Maria DB (no limits on db per inst)
    - Postgres (no limits on db per inst)
    - Aurora (no limits on db per inst)
  - can get RI w/ multi-AZ 
  RDS is not serverless
- ***Aurora Serverless is the exception*** 


### Security & Monitoring ###
- Encryption at rest for all RDS DB using KMS
  - encrypts related backups, read replicas 
- **IAM DB Authc** (works only for psql & MySQL)
  - network traffic to/from db is encrypted using SSL
  - use IAM to manage access to DB
  - STS does NOT work with RDS

- RDS provides metrics for the OS your DB runs on (unlike EC2 where cloudwatch cannot "see" into your inst)
  - provides CPU util, DB connections and memory, storage, network 
- CloudWatch gathers cpu util from hypervisor
- *Enhanced Monitoring Metrics* installs agent on the inst -> allows you to view processes/threads use DB cpu/mem usage 
  - can view OS processes, RDS child processes -> see how each thread is using CPU
- remember - you don't have direct access to RDS to write a script

### RDS Backups, Multi-AZ and Read Replicas ###
- Two Kinds of Backups: Automated or Snapshot
  - **Automated** recover db to any point in time within retention period (1-35 days)
    - when you change to auto backup - changes are immediate
    - AWS takes daily snapshots adn stores transaction logs -> when you recover you can do it to the second YYYYMMDD-hh:mm:ss
      - enabled by default 
    - Backup to S3 is free up to size of RDS 
      - 10 GB RDS -> get 10 GB free S3 storage 
    - backups occur within defined window, during that time, can experience latency/lag
    - *required* to make Read Replicas
  - **DB Snapshots**
    - manual (user-initiated) -> not managed by AWS (not automatic): you delete them 
      - similar to EBS snapshots, they live independent of RDS and not deleted with RDS delete 
    - when you restore an RDS - it will have a new endpoint 
    
- Max RDS Volume is 16TB

- RDS on Virtual Machines but YOU dont have access to machines - cannot ssh or access their OS
  - you don't patch the OS for RDS - Patching and Maintenance of RDS is Amazon responsbility

- Multi AZ -- Synchronously via Synchronous Replication (not for performance improvement) All RDS are supported 
  - secondary DB in another AZ is NOT read-only. It's an active-active deployment
  - traffic only routed to primary (active) instance
  - auto failover, auto backups from standby inst
  - failover can happen when: 
    - storage fails on primary 
    - loss of availability in primary AZ
    - loss of network connectivity to primary
    - compute unit failure on primary
  - failover means switching the CNAME from primary to standby 
- Read Replicas -- write to one database and then asynchronously copied to other replicas
  - Aysnchronously (Read Replicas can also be a way to copy Prod Databases)
  - Max of 5 Read Replicas
	- Used for scaling and perf by pointing reads to read replica NOT DR
	- Can have read replicas of read replicas (but watch out for latency)
  - Read Replicas -- supported databases (all but MS SQL Server)
    - My SQL
    - Maria DB
    - Postgres
    - Aurora
  - Each read replica has its own DNS endpoint 
  - can be promoted to its own db - can be placed in another region or AZ (when you create RR, you choose location)
  - Can reboot RDS and point to a read replica in another AZ as a failover (with RDS Instance reboot you can choose Failover node)
- ***For creating Read Replicas you need to have Backups enabled***

- How to improve performance -- Read Replicas and Elastic Cache


## Amazon DynamoDB - AWS's NoSQL DB ##
- Amazon DynamoDB is a key-value and document database that delivers single-digit millisecond performance at any scale. 
- Marketing
  - It's a fully managed, multiregion, multimaster, durable database with built-in security, backup and restore, and in-memory caching for internet-scale applications. 
  - DynamoDB can handle more than 10 trillion requests per day and can support peaks of more than 20 million requests per second.
- DynamoDB synchronously replicates data across three facilities in an AWS Region, giving you high availability and data durability. 
  - SSD based storage
  - partitioned across many nodes for perf - || processing for perf
- RDS has fixed schema where as DynamoDB(NoSQL) has dynamic schema
- NoSQL Database -- which provides consistent and single digit milli second latency
  - great for mobile, web, gaming, ad-tech, IOT
- allows for PK to be 1 or multiple attributes 
  - A) single-attribute partition key (ex: User ID) 
  - B) a composite partition-sort key (ex: User ID and Time Stamp) Composite key design lets it store related items close together on the same table.
- Two Read paradigms 
  - Eventual Read Consistency (Default)
    - Best Performance, consistency occurs after ~ 1 second 
  - Strongly Consistent Reads
    - all writes must be successful before next read ~ about 1 seoncd
    - only for cases where you need to read after write < 1 second
    - costs money

- **Pricing**
  - read/write throughput you reserve (charged regardless of whether you use it)
    - this capacity will auto-scale UNLESS you launch DynamoDB from CLI - then you have to manually scale up the read/write capacity units
  - data storage
  - internet data transfer fees

- **DynamoDB POST method**
https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Programming.LowLevelAPI.html 

###  Components & Attribute Limits  ###
![DynamoDB Components](/images/DynamoDB-components.png)

- **Tables** - Built of tables (does not have schemas, just indv tables). Each table contains 0+ items
- **Items**  - item is a group of attributes that uniquely identifiable among everything ~ row / record in a table
- **Attribute** - data element of the item ~ column 

- Attribute name [1 - 64KB]
- Item size limit is 400KB - meaning name + value (data: string/number/binary) <= 400KB

- **Partitions**
  - DynamoDB stores data in partitions (managed entirely by AWS - based on partition size and throughput settings) 
  - Partition key (single primary key) - is how partitions are stored and retrieved
  - Partition-sort key (composite PK) - will place all items with the same partition key close together (same partition) ordered by the sort value
- to OPTIMIZE performance (Provisioned I/O capacity for the table is divided evenly among these physical partitions.)
  - you want to parallelize reads and ensure query sizes do not scan unnecessary data - while remembering high throughput costs money
  - this means that you can create partition keys that "randomize" your data across various partitions to force || reads
  - this also means you can create tables for newer relevant workloads with higher throughput capacity and retire old tables to a lower throughput capacity

- **DynamoDB Streams**
  - ordered flow of info about changes to items in dynamodb
  - when enabled, it captures info about every modification (create, update,delete) will go into a stream record
  - can be integrated with AWS lambda to create triggers to events in dynamodb streams to react to certain events 

### DAX - Dynamo DB Accelerator ###
- is a fully managed, highly available, in-memory cache that can reduce Amazon DynamoDB response times from milliseconds to microseconds 
- Either you code an app to point certain reads to the cache OR you point it to DynamoDB and use DAX for write-throughs...
  - can handle multiple "All or Nothing" transactions at once (money transfers that involve complex reads to confirm and then a write commit)

- On-demand capacity - pay per request as a balance to cost/perf
  - no minimum capacity
  - if you exceed your capacity, transaction costs are higher 
- On-demand Backup/Restore
  - through Console or API call - can run full backups w/o latency
  - user-initiated, retained until deleted
  - backups stored in region (cannot backup to another region)
- Point in time recovery 
  - not enabled by default ~ RDS backups that allow 35 day retention period, incremental backups... 
  - latest restorable version is ~ 5 minutes in the past 

### Streams ###
- Streams are: time ordered sequence of item-level changes in a table - stored for 24 hrs (default)
  - FIFO data pieces and commands (insert,delete,update...)
- stream records are grouped into shards
  - can do CRR (cross region replication) 
    - used for aggregate filters, sharing data... 
    - can be combined with AWS Lambda for stored processes... 
- Stream Records & shards feed Global Tables - based on streams for globally  distributed apps
  - multi-region redundancy for DR/HA 
  - replication latency ~ 1 second 

### Security for DynamoDB ### 
- VPN site to site using Direct Connect 
- Can use IAM policies & roles to control access 
  - can limit users to access/view certain columns
- VPC endpoints to access within a secure network 
- Security group for RDS DB can autofill port - don't need to explicitly specify port/protocol



## Amazon Redshift - AWS DW for business intelligence ##
- fast and powerful fully managed petabyte scaled datawarehouse in the cloud - 1 TB storage ~$1000 per year
- Designed for OLAP capabilities & business intelligence
- Availability - currently available only in *SINGLE AZ* deployments
  - can restore snapshots to new AZ in case of outage
- Two types of Deployments: 
  - Single Node: 
    - 1 large machine for processing with a 160 GB RAM limit
  - Multi-Node: 
    - 1 Head/Leader node that coordinates connections and queries 
    - Compute Nodes - store data and perform queries/computations (max 128 nodes)

- redshift spectrum: allows you to directly run SQL against unstructured data in S3

### Functional Benefits ###
- Advanced Columnar Compression
  - multiple compression techniques and uses less space compared to traditional RDS
  - doesn't require indexes or views (less space)
  - will compress based on reading a segment of rows
- MPP - mulitple parallel processing (add up to 128 compute nodes behind leader node)
  - auto distributes data across worker nodes to spread workload across available nodes 

### Redshift Backups & Pricing ###
- enabled by default with retention period 1-35 days 
- maintains at least 3 copies of data -
  - the original on the compute node
  - replica on compute nodes 
  - backup in S3
- *Cross Region Snapshots Copy* can aysnchronously replicate your snapshots to S3 in another region for DR
  
- **Pricing**
- charged for compute node hours, storage, backups and data transfer
  - not charged for leader node 
  - Data transfer charges are only with in a VPC, not outside of it 

### Redshift Security ###
- encryption in transit using SSL
- encryption at rest AES-256 
- redshift manages keys by default -> can use KMS & HSM 
- **Redshift Enhanced VPC Routing** 
  - forces all COPY & UNLOAD traffic btw your cluster and data repositories through VPC
  - allows for VPC endpoints, NACLs, Sec groups... -> can manage flow of data and use VPC Flow logs to monitor the COPY/UNLOAD traffic
  - by default - it is not enabled and redshift routes traffic through internet as needed
- **Audit Logging**
  - records info on connections, queries run and user activities in Redshift


## Amazon Aurora ## 
- MySQL & PostgreSQL compatible Relational Database that combines the speed and availability of commercial databases with simplicity and cost effectiveness of OSS databases 
  - 5 times better performance than MySQL - if there are perf questions with MySQL - replace with aurora
  - starts with 10G and scales up in 10 GB increments to 64 TB (autoscaling storage)
    - compute 32vCPU with 244 GB RAM 
  - Data duplication: 2 copies of data in every AZ - meaning at least 6 copies of data in each region 
	- HA features: 
    - able to sustain the loss of 2 copies of data without affecting write availability 
    - can lose 3 copies of data without affecting read availability
	  - self healing - auto-scan for errors and fixes them 
	- backups doesnt impact database performance
- Has writer and reader node (sometimes in seperate AZs)
- Runs on a small cluster of DB inst
  - each connection handled by a specific DB inst, and when you point to the cluster it goes through an *endpoint* called the cluster endpoint or writer endpoint
  - cluster endpoint - only one with write ops - each Aurora DB cluster has one primary and one cluster endpoint 
    - used for (DDL - data definition language and DML - data manipulation language)
  - reader endpoint - LB for read only connections (queries) [points to aurora replicas for reads if avail]
  - It is possible to map connections to *custom endpoints* -> ec2s go here, users go there for better perf
    - direct query users here, and prod workloads there
  - inst endpoint - typically used to test issues with specific inst 



### Aurora Replicas ### 

|  Aurora   |  MySQL or PSQL  |
|-----------|-------------------------| 
|  No CRR   | Allows CRR  | 
| Automated Failover | No Automated Failover | 
| Primary DB with 1 schema | Supports different data schemas & Secondary DBs... | 
| Max 15 replicas | MySQL max (5), PSQL max (1) |

- To copy a DB to Aurora
  - **DB MUST be PSQL or MySQL**
  - Create an Aurora Read Replica 
  - Go to the writer node and promote replica 
- OR 
  - Can take a snapshot of a Aurora Read Replica in S3 
  - Restore from snapshot 

### Aurora Backups ###
- you can share Aurora snapshots with other AWS accounts
- backups are ALWAYS ENABLED - do not affect perf
- snapshots can be taken - do not affect perf 

### Serverless Aurora ### 
- Pay per invocation rather than hourly (like all RDS services) 
- on-demand, autoscales to start/stop & scale based on app needs 
- simple cost-effective for infrequent, intermittent/unpredictable workloads 
  - good for testing website -> high perf for ultra cheap - when you are unsure of demand
- on-demand, auto-scaling configuration for Amazon Aurora. An Aurora Serverless DB cluster is a DB cluster that automatically starts up, shuts down, and scales up or down its compute capacity based on your application's needs.
- Aurora Serverless provides a relatively simple, cost-effective option for infrequent, intermittent, sporadic or unpredictable workloads
-  It can provide this because it automatically starts up, scales compute capacity to match your application's usage and shuts down when it's not in use.
- you can create a database endpoint without specifying the DB instance class size. You set the minimum and maximum capacity
 
### Aurora Global Database ###
- Dsigned for globally distr apps - run single Aurora db to span multipe regions
- replicates data with no impact on db perf, fast local reads with low latency, and DR across regions
- latency of <1 sec
- Cross Region DR: RPO of 1 second, RTO < 1 min for excellent biz continuity plan

## Database Migration Service ##  
- migrate on-premise DB or AWS DB to AWS target: RDS or EC2
- cloud server in AWS running replication software 
  - source DB feeds source endpoint
  - Replication task instances that replicate out to
  - Target endpoint which feeds target DB 
- DMS can build target definitions based on source OR
- you can define target mappings (tables, indexes, views...)

- Supports homogenous migrations (ORACLE -> ORACLE)
- Supports certain heterogenous migrations (MySQL -> Aurora)
  - required to run  schema conversion tool (optional for homogenous)

- Supports sources such as: 
  - RDS & On-Premise RDS types + MongoDB, SAP & DB2
  - Azure SQL 
  - AWS S3
- Supports targets such as:
  - RDS, Redshift, DynamoDB, ElastiSearch 
  - Document DB, Kinesis Data Streams
  - AWS S3 
  - On-premise inst (but NOT: Mongo or DB2 )




# Caching in AWS #

## Elastic Cache ##
- web service memory cache to improve performance of web applications and database by storing recent query results
  - that way - db query gives answer, and now cache can serve up same answer instead of redoing joins and sending request to DB
- Supports two kinds:
  - memcached (simple, multi-threaded and horizontal scaling) 
  - redis (advanced like multi AZ) 
    - has advanced data types
    - rank/sort data
    - publish/subscribe abilities 
    - persistence
    - backup/restore capabilities  
- **Redis**
  - Redis AUTH command can improve data security by requiring a user to enter a password before they are granted perms to execute redis commands 
  - to require users enter a password - use the --auth-token
  - for encryption in transit --transit-encryption-enabled

## IF Database is overloaded -- create read replica AND/OR use elastic cache
- CloudFront -> Caches origin obj, if avail, will provide, if not it queries for it from source 
- API Gateway
- Elasticache (memcached or Redis)
- Dynamo DB Acc (DAX) 

- More caching up front, less lag
- Can string multiple of these together with CloudFront and then having API Gateway feeding requests to Lambda/EC2 which queries DB like dynamo DB and/or elasticache
- You can store session state data on both DynamoDB and ElastiCache.  These AWS services provide high-performance storage of key-value pairs which can be used to build a highly available web application.

# AWS EMR - Elastic Map Reduce # 
- Cloud Big Data Platform to process OSS - Apache Spark, Presto, HBase, Hive, Flink, Hudi
- AWS is faster/cheaper than on-premise
- **it can pull directly from s3 so it is great for analyzing gobs of logs in s3**
- Cluster of VMs with various node types (Different software on each node for a "role")
- Node Types:
  - Master Node:  Manages cluster, tracks status of tasks, monitors health... [required]
    - Logs are stored on /mnt/var/log by default 
    - if you need log data to persist, can schedule a backup to S3 for 5 min intervals
      - only configured upon setup and cannot be changed while cluster is running
  - Core Node:  Runs tasks AND stores data in HDFS
  - Task Node:  Runs tasks ONLY (does NOT store data)
- to improve perf: reduce input split size and adjust number of simultaneous mapper tasks so more tasks can be processed at once

# AWS Directory Service # 
- Family of Managed services that allows you to connect to AWS resources using corporate AD -OR- stand-alone directory in the cloud
  - allows SSO to any domain-joined EC2 inst 
1.  **AWS AD Services** - Your AD trusts AWS AD - managed MS AD
2.  **Simple AD** - Mini AD in AWS 
3.  **AD connector** - AWS proxies to your AD 

### AD Background ###
- AD is an on-prem directory service for authentication supporting protocols such as: (Kerberos, LDAP, NTLM, & DNS)
- Provides hierarchy of users, groups, servers in trees & forrests 
  - uses groups policies 
  - typically HA 
- takes a lot of manual work to upkeep & admin 

## AWS Overview of Directory Services ## 

- AWS provides Domain Controllers (DC) within AWS's VPC so your resources in your VPC can authenticate against it. 
- You exclusively access DC
  - can extend to on-premise AD to trust AWS managed DC

|  Customer Responsibility  |  AWS Responsibility   | 
|---------------------------|-----------------------|
| Users/Groups & Group Policies | Multi-AZ deployment | 
| Std AD tools | Patch, Monitor, recovery |
| scaling out AD | Instance Rotation | 
| Trusts (resource forest) | Snapshot & Restores | 
| Certificates (encryption) |    | 
| Federation |   | 

## Simple AD ## 
- standalone AD in AWS with basic AD features 
- 2 offerings: Small < 500   OR   Large < 5000 
- Easier than setting up EC2, installing AD and managing it yourself... 
- Ideal for linux jobs that need AD authentication 
- DOES NOT support trusts 

## AD Connector ## 
- Directory Gateway (proxy) for on-premise AD 
- used for on-premise directory with compatible AWS services 
- allows on-prem users to logon using corporate AD 
  - can join EC2 inst to existing AD domain and add as many as you like 

# NOT AD services # 
- These services have AD sounding names, but they are not related. Need to know the difference and not get confused
- Cloud Directory - Hierarchical storage for software devs 
- Cognito User Pools - Mobile/Webapp Login

## Cloud Directory ## 
- Directory/hierarchy storage for software devs
  - can store deep hierarchies with many children 
  - Org charts, course catalogs, device registries... 
  - fully managed, no infra

## Amazon Cognito User Pools ## 
- Managed user directory for SaaS apps 
- for sign-up/sign-in web/mobile apps 
- Works with social media ID 

# IAM Policies # 
## ARN - Amazon Resource Name ## 
Structure of an ARN: 

>   arn  :  partition  :  service  :  region  :  ACC ID  :  [ resource_name/qualifier | resource_type : resource_name/qualifier ]    
>   Example User Marc:  arn:aws:iam::12345:user/Marc    [missing region because IAM is global]   
>   Example S3 Obj:     arn:aws:s3:::bucket_A/image.png [missing ACC because bucket name is unique]  
>   Example EC2 Inst:   arn:aws:ec2:us-east-1:12345:instance/*  [full everything, using regex to select obj]  

## IAM Policies ## 
- JSON doc defining privileges - list of statements
- Two kinds of policies: 
  - ID Policy: Tied to a user/group/role - permissions on what principal can do 
  - Resource policy: tied to a resource - who has access and what action can they perform on it 
    - no effect until attached to an obj 
- statements in JSON policy match API requests: 
  - Effect: [Allow | Deny] which applies to following obj privileges
  - Action: [arn list of services with an action name] 
    - follow convention service_name:action_name
    - dynamodb:Delete*
    - dynamodb:Update*
  - Resource: arn:aws:dynamodb:*:*:table/A
- Two types of policies:
  - AWS Managed Policies: OOB policies with orange boxes 
  - Customer Managed: custom built by/for you 
- **Role**: essentially a mapping of obj to a policy(s) -> "hat"
- Inline Policy: custom code added to a role to further modify policies currently applied
  - typically for testing rather than std governed stuff 

### Tips ### 
- Not explicitly allowed = implicitly DENY
- Explicit DENY overrides ANY ALLOWS
- Only attached policies have effects 
- multiple policies are joined, then evaluated 

## Permissions Boundaries ## 
- For delegating admin access to others -> Supports AWS Users & Roles 
- Advanced feature that prevents privilege escalation or unnecessarily broad privileges
- MAX privileges for a user/role
  - AppDevs can create roles for EC2 inst, but they cannot create an admin role 

# AWS Resource Access Manager # 
- define shared resources between multiple ACC or AWS Orgs
  - reduces duplication of: 
    - AppMesh, CodeBuild
    - Aurora, EC2 inst
    - Route 53, License Manager
    - Resource Groups...
- Used for Account isolation or multi-acc strategy 
  - different aws acc around billing, admin... to limit security breaches 
- Example: Launch EC2 inst in common subnet (ACC1 shares subnet so ACC2 can launch EC2 in it)
- Steps to share resources:
  - ACC1 shares resource with ACC2 in Resource Access Manager
  - ACC2 goes to Resource Access Manager and ACCEPTS resource
  - ACC2 now can go into console and view resource

# AWS SSO - if you see SAML think AWS SSO # 
- centrally manage access to AWS ACC & biz apps
- *can use existing corporate ID* "They want all *users* to access resources using their on-premises credentials, which is stored in Active Directory."
- can define account level access - admin team has full perms, but DEV team has readonly
- operates within AWS VPC
  - can do outbound authentication to business apps using SAML 2.0 protocol -> Azure AD 
  - Access to org units DEV vs PROD 
  - All signons tracked in CloudTrail 
- Sign into corporate ID & AWS authenticates out to business apps, AD, etc using existing ID to sign onto other apps (salesforce, IBM...) 
  - not for EC2 login 


## SAML Steps ##
- https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_providers_saml.html    

![AWS SAML Picture](/images/saml-based-federation.diagram.png "AWS SAML Picture")

1. A user in your organization uses a client app to request authentication from your organization's IdP.
2. The IdP authenticates the user against your organization's identity store.
    - the portal first verifies the user's ID in your ORG then generates a SAML authc response
3. The IdP constructs a SAML assertion with information about the user and sends the assertion to the client app.
4. The client app calls the AWS STS (security token service) *AssumeRoleWithSAML* API, passing the ARN of the SAML provider, the ARN of the role to assume, and the SAML assertion from IdP to generate temporary tokens.
    - after the client browser posts the SAML assertion, AWS sends the sign-in URL as a redirect and the client browser is redirected to the console
5. The API response to the client app includes temporary security credentials.
6. The client app uses the temporary security credentials to call Amazon S3 API operations.




# Route53 #
- from Interstate 66 - but DNS port is default 53 so a punny name for DNS service 
- "Extends" DNS functionality because you can point to AWS resources (not just IP addresses like typical DNS)

## DNS Basics ##
- DNS is basically a phone book, given a name - here's the IP address #
- ipv4 is 32 bit and ipv6 is 128bit 
- Top Level Domains: ".com" - All domain names managed by IANA (Internet Assigned Numbers Authority)
  - all names need to be unique and no duplication. Can register through registrars such as: GoDaddy, Amazon, others...
- **SoA Record**
  - Start of Authority - stores data about instantiation of a name:
  - name of server that supplied data for that zone 
  - Admin of the zone
  - current version of the file
  - TTL of resource 
- **NS Record**
  - used by top level domain servers to direct traffic to correct Name Server -> contains authoritative DNS records (SoA record) 
- **A Record**
  - Address Record - that translates google.com to IP address
  - contains TTL for DNS record is cached on resolving server or local PC
    - lower TTL - faster changes to DNS records propogate - but also means they hit your server more often.  
- **How it works** 
![DNS Resolution](/images/DNS-resolution.png)
  - User accesses Google.com in browser 
  - routed to Top Level Domain Server ".com" and says talk to Name Server "Google"
  - Name Server "Google" holds the records - SoA and the A record containing the IP address to go to 
  - IP address is passed back to client 

- *CNames - Canonical Names*
  - used to resolve 1 domain name to another "redirect" - mobile.google.com -> google.com 
  - Sub domains
  - can NOT be applied to google.com  - only SUB domains like mail.google.com
- *Alias Records - AWS enhanced A-record*
  - mapping resource record sets in VPC to ELB, CLoudFront, S3...
  - benefit of route 53, this is an AWS enhanced DNS functionality that allows DNS to point to AWS resources
    - can be used at the zone Apex (whereas CNAME records cannot)
    - can also point to AWS resources hosted by other accounts by entering the ARN
  - Elastic Load Balancers (ELBs) doesnt have ipv4 address and they are accessed through DNS Name 
  - AAAA record set is IPv6
  - A record set 
  - *Aliases preferred to Cnames*
- *PTR Record* - reverse of A Name - given this IP, here is the Domain Name

- Can register domains in AWS with Route 53 -> default max of 50 (but can increase by contacting AWS TS)
  - can manually add cnames and arecords... can take up to 3 days to register 


### EC2 IP Addresses ###
- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-instance-addressing.html#concepts-public-addresses 
- public ip addresses are mapped to the primary PRIVATE IP address through NAT
- DNS (ec2-203-1-1-1.compute-1.amazonaws.com) -> public IP -> private IP in vpc
  - because of this, you cannot manage public IP on inst - assigned at random by AWS. for persisten public IPs, use elastic IP
- when you start/stop EC2 inst - the IPv4 address changes automatically 
- can break IP tables & Route 53 if you have hard-coded references
- can get around this by using Elastic IP (AWS provisioned/reserved IP addresses) OR ELB
- **Elastic Ip**
  - is a persistent public ipv4 address which is accessible to internet. You need to get an Elastic IP and assign it to your instance.
  - You can mask the instance failure by rapdidly assigning to another instance in your account.
  - Once Elastic Ip is assigned your public ipv4 address is released to pool 
  -  you cant make public ipv4 address to elastic ip


## Routing Policies in Route 53 ## 
- Routing Policies available on AWS
- All but Simple have Health checks which remove target after certain number of failed tries and put it back after successes, and these health checks can be tied to SNS
1.  Simple Routing  
    - (NO Health Check) 
    - 1 A-record with multiple target IPs (IPs sent to requesting client at random)
2.  Weighted Routing
    - split traffic based on weights assigned (80% us-west, 20% Sydney)
    - create A-records for each individual target IP with assigned weight
    - weight is absolute value - and averaged across all weights provided 
    - INCLUDES HEALTH CHECK
      - can set up health check & upon failure can omit target IP until it is healthy again
    - can be used as manual scalability as you push traffic to a second inst as first one is overloaded
3.  Latency-based Routing
    - routes based on which IP has fastest traffic time
      - doesn't always mean its physically closest 
    - create A-records for each target IP and select latency
    - Can include health check
4.  Failover Routing
    - Create DR --> active/passive or hot/cold
    - MUST use health check to work 
      - if all primary resources are unhealthy, then Route 53 will direct to healthy secondary resources
      - can register domain name or IP to monitor for health check
      - for health check to succeed, route table and nacls/sec groups must allow inbound traffic from IPs Route 53 health checkers use
    - create A-record for failover primary and failover secondary
5.  Geolocation Routing 
    - Choose where traffic goes based on geo-location of users
      - Euro -> Euro Server, US goes to US server
    - Create A-record for each indv IP and GeoRegion
      - recommend creating a "default" IP & region as a catchall or you will not get a response
6.  Geoproximity Routing (Traffic Flow only)
    - Using AWS Traffic Flow - can define routes based on Geolocation, using biases and various weighting based on GPS coordinates 
    - can take into account inst location AND users vs Geolocation only decides based on user location
      - look for weights of users from regions
7.  Multivalue Answer Routing (implies Simple Routing with Health Check)
    - Same as simple routing but with a Health check 
    - create a-records with health checks 



# VPC - Virtual Private Cloud #

- lets you provision a logically isolated section in AWS where you can launch resources in virtual network that you define. 
- You can define range of IPS, subnets and creation of route tables and network gateways
- VPC standard structure:
  - External traffic into VPC comes from:
    - IGw - Internet Gateway (internet traffic)
      - Egress Only IGw: Prevents IPv6 from initiating a connection into VPC and allows VPC IPv6 based traffic to comm on internet
    - VPG - Virtual Private Gateway (used to tie VPCs or VPN)  
  - Traffic flow goes to Router and depending on where it is coming from and going it will go through the route table
  - Route Tables define destination and targets that can either be IP ranges, AWS services, etc... 
    - can be the location you deny public internet access or allow it through a NAT gateway 
    - tied to subnets - each subnet MUST be tied to ONLY one route table
    - Only allow 1 source, 1 target - if you want to configure 1 source to multiple targets need to create separate rules for each target
  - NACLs then filter the traffic by type, port, etc... Firewall for your subnet 
  - Security groups tied to EC2 inst - firewall for your EC2 inst
  - Internal resource (EC2 inst or RDS...)
- VPC allows
  - launch EC2 inst in VPC/subnet of choosing 
    - in a new VPC the default settings - they only provide private DNS hostname - to get public DNS, need to enable DNS resolution/hostname
    - in the default VPC (one auto created with acc) AWS provide public/private DNS hostnames that correspond to the public/private IPs
  - Assign custom IP ranges in subnets (routing prefixes 16-28)
    - subnets are tied to 1 AZ (cannot stretch across multiple AZ)
  - configure route tables btw subnets 
  - create IGw
  - Security groups
  - Subnet NACLs

- Default VPC (DO NOT DELETE!)
  - user friendly for basic use meaning:
  - all subnets have route out to the internet
  - each EC2 has public AND private IP

- VPC Peering
  - connecting VPC to another using direct network route (can peer across regions and across AWS ACC)
  - **No Transitive Peering** :: VPC can only see what it is directly connected to - VPC1 cannot see "through" VPC2 to VPC3  
  - **does NOT support Edge to Edge Routing** :: meaning if VPC-A peered to VPC-B, and VPC-B has IGW & Direct Connect, VPC-A can NOT access internet or on-prem stuff 
  - peering related connections include:
    - VPN connection or AWS Direct Connect to on-prem
    - VPC Peering
    - Internet Gateway
    - Internet connection through NAT
    - VPC Endpoint to another service
    - IPv6 classiclink
  - if you see ANY question involving 2+ VPC and on-premise talking about multiple connections - REMEMBER TRANSITIVE PEERING


## Creating a VPC ##
- When VPC is created it will automatically create: 
  - Route Table (default allows all internal traffic within subnet)
  - Network ACL (default NACL allowing all inbound/outbound traffic - fallback for subnets to implicitly tie to, unless explicitly tied to another NACL)
  - Security Group (default allows all internal traffic and any outbound) 
    - do not span VPCs
- Need to manually create Subnets & IGw
  - Subnet tied to 1 AZ - cannot span multiple AZ
  - IGw - **ONLY 1 IGw PER VPC** 
    - Adding IGw is NOT enough to access internet 
    - need to configure route table to allow internet traffic from within VPC to target IGw
      - Recommended to create 2nd route table with public route and keep default one private 

### Reserved IP Addresses ###
- AWS only allows /16-28 cidr ranges for VPCs
- Reserved VPC addresses:
  - 10.0.0.0 - 10.255.255.255 (10/8)
  - 172.16.0.0 - 172.31.255.255 (172.16/12)
  - 192.168.0.0 - 192.168.255.255 (192.168/16) 

- The first four IP addresses and the last IP address in each subnet CIDR block are not available for you to use, and cannot be assigned to an instance. 
  - 10.0.0.0: Network address.
  - 10.0.0.1: Reserved by AWS for the VPC router.
  - 10.0.0.2: Reserved by AWS. The IP address of the DNS server is the base of the VPC network range plus two. 
  - 10.0.0.3: Reserved by AWS for future use.
  - 10.0.0.255: Network broadcast address. We do not support broadcast in a VPC, therefore we reserve this address. 

## NAT inst vs Gateway ## 
- Network Address Translator - a proxy for private subnets being able to communicate to internet 
- Two kinds of implementations:
  - **NAT Instance** is specific to EC2 instance for getting over to Internet Gateway (are being deprecated) 
    - you manage it, and can configure it to be HA across AZ or autoscale... 
    - need to disabe source/dest check on inst
    - must be in public subnet 
    - must have a route from private to public subnet (so private resources can contact it)
    - Depending on workload - can be limited by EC2 inst size 
    - *as an inst - it is behind security group*
  - **NAT Gateway** is Highly Available not just one insatnce and cover multi AZs  
    - redundant within AZ 
    - 1 NAT Gateway per AZ (can configure in HA by routing to avail NAT)
    - auto-assigned Public IP
    - still need to update route table to point private internet traffic to NAT Gateway
    - no need to disable source/dest checks 

## NACLs ##
- Take effect immediately 
- New NACL by default denies all inbound & outbound traffic 
- Has implicit and explicit denials (compared to security groups which only have allows) 
  - allows ability to block certain IPs 
  - stateless so you must explicitly open in/out traffic (ssh - comes in port 22, communicates out ephemeral ports...)
- NACLs evaluate rules in order 
  - evaluates in order of rule ID - earlier the rule higher the precedence
  - 99 deny > 100 allow > 400 deny 
- Custom NACLs default block all in/out 
- NACLs are evaluated before security groups 
- NACLs are tied to subnets - but subnets can only have ONE associated NACL
  - subnets must be tied to NACL - and without explicit assoc, auto assigned to default NACL 

## VPC Flow Logs ##
- capture info on IP traffic from NICs in VPC
  - captured in Cloudwatch logs OR sent to S3
- Create a flow log and retrieve from CloudWatch 
- 3 Levels of logs:
  - VPC
  - Subnet
  - NIC

- Restrictions
  - cannot enable flow logs for VPCs not under your acc (peered VPC)
  - Can tag flow logs 
  - after flow logs created - cannot change config or IAM role
  - Not all traffic is logged - some are filtered out:
    - Amazon DNS comms are filtered out
    - windows license activation
    - inst metadata comms... 
    - DHCP traffic, VPC Router...

## Bastion Hosts/Jump Boxes 
- special purpose server on network to withstand attacks
- typically a proxy server outside firewall or DMZ accessed by untrusted networks
- "a way in" to the private network
- VS a NAT which is "a way out" to the public network (providing internet to private subnets)
- Should be SMALL as it is not supposed to do any processing

### HA Bastions ###
- 2 ways to do HA:
  - **(A)** can spread 2 indv ec2 inst across different AZ and route traffic using Network LB 
    - need Network LB because ssh is not L7 comm (cannot use App LB or Simple LB)
    - Network LB is expensive and you have 2 ec2 inst on at all times
  - **(B)** 1 EC2 inst inside an autoscaling group of min=1 across 2 target groups + Elastic IP tied to Ec2 inst and a user data script to assign Elastic IP
    - downtime can be longer depending on health checks and EC2 startup - but its WAY cheaper



# AWS Direct Connect # 

- Private dedicated network connection from on-prem DC to AWS region
  - reduces network costs 
  - Increase bandwidth/throughput
  - Consistent exp (internet connection stability is highly variable)

**How to set up Direct Connect**  

>   Tl;dr: Create a Public Virtual Interface in Direct Connect, create 2 gateways: a Customer Gateway and a Virtual Private Gateway, attach the Virtual Private Gateway to your VPC, create a VPN connection selecting the Customer Gateway and the Virtual Private Gateway, once the VPN is available, take the IP addresses from Tunnel Details to configure the on-prem Firewall and VPN.

- [For those coming from ACG, here are the timestamps:](https://www.youtube.com/watch?v=dhpTTT6V1So)
  - 2:08 : Point 1: Create a Public Virtual Interface and wait for it to be available
  - 4:40 : Point 2: In the VPC console, go to VPN Connections > Customer Gateways and create a Customer Gateway (in the video, it’s named VPN Firewall)
  - 5:02 : Point 3: In the VPC Console, go to VPN Connections > Virtual Private Gateways and create a Virtual Private Gateway (In the video the name used is DEV-VPC-VGW)
  - 5:23 : Point 4: Attach the DEV-VPC-VGW (Virtual Private Gateway) to your VPC and wait for it’s status to change to attached. (Please note that you need to have a pre-existing VPC AND a pre-existing DirectConnect connection as well. These steps just make the DirectConnect Connection encrypted by using VPN to prevent eavesdropping)
  - 6:03 : Point 5 & 6: Create a VPN connection from VPC console using the Virtual Private Gateway (In the video, it’s DEV-VPC-VGW) and the Customer Gateway (named VPN Firewall). In the video the VPN connection is named DEV-VPC-VPN. You need to wait at the console for the VPN connection’s state to change to available (including the long wait at the spinning loader as shown in the video)
  - 7:36 : Point 7: Use the IP addresses in the VPN Connection’s Tunnel Details to set up the VPN on your Firewall side (on prem)
- Above establishes 1-way to allow on-prem to connect to Amazon VPC
- There is no route connecting your VPC back to the on premise data center. You need to add this route to the route table and then enable propagation on the Virtual Private Gateway.

## VPN Connection ##
- **REMEMBER - NO TRANSITIVE PEERING: from VPC to VPC or even on-prem->VPC->VPC**
- Components:
  - need an ASN for Virtual Private Gateway
  - a private subnet in VPC
  - a vpc with hardware vpn access (hardware VPN over internet btw VPC and on-prem)
  - an on prem customer gateway
      - DO NOT need virtual customer gateway 

### Setting up access from VPC -> on-premise ###
- by default, inst in VPC cannot talk to on-prem 
- you enable on-prem access from VPC by:
  - attaching Virtual Private Gateway to VPC
  - creating a custom route table, 
  - updating security group rules
  - creating an AWS managed VPN connection 
- Customer Gateway is an on-premise physical device or software app
  - Customer Gateway in AWS is a logical definition about your on-prem Customer Gateway
  - NEXT you need to set up a static IP of the customer gateway's external interface (singe point of contact)

- this way - you have a VPGw on your VPC to open VPC to on-premise VPN
- on-prem Customer gateway is physical and has a logical component in AWS to store its metadata
- you route any traffic from vpc to vpn to your virtual private gateway and it sends it to the customer gateway 
![AWS Basic VPN](/images/Basic VPN setup.png "Basic VPN")

### Setting up access from on-premise -> VPC ### 
- NOT direct connect but: route 53 cannot be read by external sources (internet or customer vpn)
  - work around is to create EC2 hosted DNS inst that does zone transfers from internal DNS and can be queried by external servers

## VPC Endpoints ##
- lets your AWS resources (ec2, etc..) communicate to AWS services over AWS network instead of over the internet
- privately and quickly connect VPC to AWS stuff or another VPC endpoint run by private link
  - don't need NAT, VPN or Direct Connect
  - when you create a VPC endpoint, you can attach an endpoint policy that controls access to the service you are connecting to (controls access from specific endpoint to specified service)
    - does not override existing S3 bucket policies... 
    - rather than set up an indv policy on each s3 bucket allowing this endpoint, can set up an endpoint policy to grant access to your various buckets
- horizontally scalable, redundant & HA... 
- Come in 2 kinds:
  - *Interface Endpoints* 
    - ENI attached to EC2 with private IP address as entry point for all traffic to (Cloudwatch, Event, Logs, API, KMS, SNS, SQS, Kinesis...)
    - basically communicating with AWS services on AWS network
  - *Gateway Endpoint* 
    - Allows you to talk to S3 and DynamoDB without traversing internet
    - communicate with AWS services that have public DNS over AWS Backbone 


# AWS Global Accelerator # 
- Basically a way to traverse AWS backbone to find your app endpoints instead of using Internet to access various app endpoints 
  - "anycast static IP" "traffic should bbe routed to nearest app endpoint to the client"
- Global Accelerator improves performance for a wide range of applications over TCP or UDP by proxying packets at the edge to applications running in one or more AWS Regions. 
- Global Accelerator is a good fit for non-HTTP use cases, such as gaming (UDP), IoT (MQTT), or Voice over IP, as well as for HTTP use cases that specifically require static IP addresses or deterministic, fast regional failover.
  - for static and dynamic content - over HTTP use CloudFront
- improves performance by directing traffic to optimal endpoints over the global network 
- Provides you with static anycast IP addresses that serve as a fixed entry point to your applications hosted in one or more AWS Regions
  - if you used internet - you bounce across ISPs and networking providers to/from AWS vs AWS only 
- Communication train:   User -> Edge Location -> Global Accelerator -> Endpoint Group -> Endpoint 
- Global Accelerator directs traffic to optimal endpoint based on a function of health, proximity and weights 
  - can spread traffic based on endpoint group dial and weights to each endpoint 

## Components of the Accelerator: ##
- **Static IP Address:**
  - by default provides 2 static IP addresses you associate with accelerator (can give your IPs if you don't want AWS defaults) 
  - Entry point into Accelerator 
- **Accelerator Network**
  - improves availability & Perf
  - 1 + listeners 
  - assigned default DNS that points to static IPs 
- **Network Zone** 
  - services static IPs for accelerator from a unique IP subnet ~ AZ 
  - isolated unit of hardware for HA - so if 1 IP goes down, can send to another IP in another network zone 
- **Listener**
  - processes inbound connections from clients to global accelerator based on port/port range
    - supports TCP/UDP
  - Each listener has 1+ endpoint groups 
  - Associate endpoint groups with listeners by specifying regions you want to distribute traffic to
- **Endpoint Group** 
  - ~ AWS Region - contains 1+ endpoints in a particular region 
  - can increase/decrease % of traffic to endpoint group using traffic dial - assists with blue/green deployments
- **Endpoints**
  - Network LB, App LB, EC2, Elastic IP
  - for each one you assign weights and specify proportion of traffic to each one





# Data Center & VPC Connection Services #

## AWS PrivateLink ## 
- Used for peering 10s, 100s, or 1000s of VPCs together OR opening up 1 VPC app/service to many many other VPCs! 
- exposes services to variety of traffic without peering, route tables, NATs, or IGw s...
  - uses Network LB on service VPC & ENI on "client" VPC
- Bad alternatives:
  - Open VPC to internet - security issues...
  - Open VPC peering - but if you have many VPCs, it can become tough to manage


## Transit Gateways ## 
- simplify your network Architecture - single point for all connections to talk to, using a hub & spoke model 
  - For on-premises connectivity, you need to attach your AWS VPN to each individual Amazon VPC. This solution can be time-consuming to build and hard to manage when the number of VPCs grows into the hundreds.
- If you have many AWS ACC, multiple regions, multiple VPCs, multiple direct connects or VPNs...
  - use Transit Gateway to create transitive peering across thousands of VPCs & on-premise Data Centers
  - acts as a Regional virtual router for traffic flowing between your virtual private clouds (VPC) and VPN connections
  - Supports IP Multicast
  - regionally based, but can configure to multiple regions and across accounts using Resource Access Manager


## VPN Cloud Hub ## 
- Hub n Spoke for VPN multiple connections - allowing multiple on-prem data centers to VPN to same AWS VPC 
- EVEN IF each datacenter has their own individual VPN connection
- only for VPNs not VPCs



# Networking Costs & Summary #
- Connecting to AWS & uploading data TO AWS is FREE
- Private IP comms within same AZ is FREE
- Private IP comms to different AZ, Region costs $$$
- Public IP comms (pulling data out of AWS, or talking to AWS services over public IP) costs $$$

- In general - use private IPs (instead of public IP) to save costs and use AWS Backbone network as much as possible
  - putting all EC2 inst in 1 AZ on private IPs = free network 

## Summarizing Notes: ##
- when you create a VPC, create a public subnet, and create an inst, but forget public IP - easiest way to make it public is to assign Elastic IP to it 
  - default VPC has a public subnet that auto-creates public IPs in that subnet
  - public subnet: has at least 1+ routes defined in route table to IGW
- Certain vulnerability/penetration tests from clients require alerting AWS 
- By default, custom VPC, new subnets, new inst can talk to one another across AZ - (all internal comms allowed by default)
- Subnets need public route table before you can access the instance, regardless of elastic IP
- VPC peering can occur between any VPC (even if VPC is under a different account)
- CIDR ranges work in reverse - "/24" means 24 bits reserved ~ 251 addresses (256 - 5 reserved AWS IPs)
  - "/16" means 16 bits reserved ~ 65,000 addresses 
- NACLs <--> subnets
- Security Groups <--> instances 
  - allow all outbound communication by default 
  - If 2 instances are in separate Security Groups they wont allow access to each other. 
  - can map traffic to/from another security group in the console
- Default Max VPC per region: 5



# Elastic Load Balancers # 
- You need atleast 2 Subnets to create a load balancer (if public facing, at least 1 subnet should be public) - after inst type, needs target subnets to deploy targets
- Load Balancers
  1.  **Application Load Balancer** (HTTPS or HTTP and intelligent) Layer 7
  2.  **Network Load Balancer** (for extreme performance) Layer 4 [Fixed IP Address - all others use DNS]
  3.  **"Classic/Simple" or Elastic Load Balancer** (simple round robin to inst) 
      - We need to enable X-Forwarding-For Header to get end user ip address with LBs and allow your EC2 app to store it 
- Error 504 is application issue and check the application not LB
- can point directly to Lambda
```Read ELB FAQs for Classic Load Balancers (5-10 questions)```

- *You cannot convert one load balancer type into another.*
- **Target Groups** LB thing
  - way to group inst, IP addresses or Lambda functions
  - allows for App LB to have advanced if-then statements and intelligent routing
  - inst use health checks and track whether 'in-service' or 'Out of service'
- can produce access logs that go to S3
- public facing LB must be tied to at least ONE public subnet to operate in

***Classic LB and Application LB always get a DNS name but no ip address
***Network LB can get a static IP

## Advanced Load Balancer Theory ##
- **Sticky sessions**
- for Classic LB enable you to 'stick' or BIND to the same EC2 Instance
  - This will be useful when you have data local to the instance
  - while session is active, all traffic goes to that inst
- Sticky Sessions for Application LB route you to Target Group
- if you find out all load going to 1 EC2 inst - disabling sticky sessions is one way to spread load

- **Cross Zone Load Balancing** 
  - enable you to distribute the load across multiple AZs - by default 
  - if classic LB in AZ1 works, but AZ2 has inst not working, you need to enable cross zone LB
- **Path patterns**
  - allow you to route the traffic based on URL path 
  - used by App LB because it can read the path
  - can be used to route images/video to another target 


## Auto Scaling ##
- select EC2 inst, VPC & subnets(allow you to spread to different AZ) to deploy -- then type of scaling Up/down, etc
- Components:
  - Auto-scaling groups (min size, max size, desired size)
  - groups: logical grouping mechanism
  - Config template: which EC2 should be launched - launch template for EC2 inst (AMI ID, inst type, security groups, key pairs...)
    - when you create a new AMI, you update the launch config 
    - only 1 launch config per AutoScaling Group (cannot be changed)
    - to "update" you create new launch config and udpate autoscaling group
  - Scaling Options: Criteria on config scaling conditions (schedule, %utilized...)
    - No scaling: keep current inst # same at all times (idempotent) AWS runs health checks
    - Scale manually: you tell AWS to scale up or down (on/off traffic like batch or analysis)
    - Schedule: 9-5 (cyclic traffic)
    - Scale based on demand [target tracking scaling] - using scaling policies, define params to scale up to max or down to min 
    - Simple scaling: choose one scaling metric and threshold values for CloudWatch Alarms that trigger the process - meant as advanced policy on top of target tracking
      - set up high/low thresholds for cloudwatch alarm and define # of inst to add/remove
      - step adjustments alter capacity of autoscaling group (and vary based on size of alarm)
      - problem is - there is a cooldown period - when cloudwatch alarm triggered, and scale up/down, there is a 
        - cooldown forces autoscale to temporarily stop scaling up/down for a default period of 300 sec (5min) to allow previous scaling activity to take effect before it responds to another alarm
    - step scaling is more advanced simple scaling, multiple  items and can vary degree of scaling with adjustment and alarm
    - predictive scaling - scale up and down based on prior workloads 

- When you delete autoscaling group, it deletes inst as well
  - also, it automatically deploys across multiple AZs for you 
- Auto Scaling can check health of EC2 or ELB
- Auto Scaling Termination Policy -- closest to billing hour or using oldest launch configuration will be terminated

### Autoscaling Default Termination Policy ###

1. Choose AZ with most inst THEN among those inst in the AZ:
2. Determine whether any of the instances use the oldest launch template/configuration 
3. Determines which instance is closest to the next billing hour
4. Otherwise - random 

- NOT - longest running, randomly terminate or # of user sessions...
- you can create your own custom termination policy based on: newest, oldest inst or oldest launch config or closets to billing hour 

### monitoring
- does health checks for EC2, and can do for ELB and even custom health checks while active (not during standby)
- sends to cloudwatch metrics
- can send events to Cloudwatch events when launch/terminating inst or lifecycle action 
- can also send SNS 
- Cloudtrail logs are stored on all calls made to API



# Cloud Formation & Elastic Beanstalk # 
## CloudFormation ##
- completely scripting your cloud environment using JSON or YAML for advanced envm - very powerful 
- FREE! - only charged for resources it builds 
- quick stats are cloud formation template allows you to build complex envm easily 
- CloudFormation Template structure:
  - AWSTemplateFormatVersion 
  - Description
  - Metadata
  - Parameters
  - Rules
  - Mappings
  - Conditions
  - Transforms
  - Resources
  - Outputs
- for build dependencies (ensure App A starts before building App B) use "CreationPolicy"
  - prevent a resource from reaching complete until CloudFormation receives specified # of success signals or timeout has passed
  - use cfn-signal helper script
  - ONLY works for EC2 inst, Autoscaling Groups & CloudFormation WaitCondition


## Elastic Beanstalk ## 
- For quickly deploying and managing applications in AWS with out worrying about the infrastructure(EC2, S3, docker containers Auto Scaling...) that runs it 
- for developers - AWS has built in templates to start a DEV environment where you can upload your code 
  - It automatically does capacity provisioning, scaling, load balancing and application health monitoring
- EC2, Elastic Beanstalk, Lambda(Serverless) is part of compute section in AWS
  - You dont need to know anything about AWS Environment for using it
  - You can use Auto Scaling with Elastic Beanstalk

# On-Premise Services & Checkpoint # 
- Database Migration
  - supports homogenous/heterogenous migrations 
  - can allow for a DR in AWS, with primary on-premise
- AWS Outpost
- VM Import/Export
  - migrate apps into EC2, as a DR or backup 
  - export AWS VMs back on-prem
- Server Migration Services 
  - incremental replicaiton into AWS (storage gateway)
  - use as a backup or DR tool 
- App Discovery 
  - helps enterprise customers plan migration projects by gathering info on on-prem apps, etc
  - install as a VM, track server util%, etc and build dependency map 
  - can bring as a csv into AWS to help calculate potential cost 
- Download AWS Linux ISO

## Review ##
- Availability: likelihood resource responds when accessed
- Durability: likelihood resource exists until user removal 
- Resiliency: likelihood system can heal or recover after damage/event
- Reliability: resource will work as intended/designed 

# Simple Services #
## SQS - Simple Queue Service (think "decouple") ##
- is a web service that gives access to message queue that can be used to store messages while waiting for a computer to process -is the first AWS Service
  - think e-mail inbox - bunch of people send you messages and you check or "poll" them and address them one at a time 
    - A queue is temporary repository for messages that are awaiting processing
  - messages can be up to 256KB of text in any format -- can go up to 2GB which are stored in S3
  - AWS guarantees messages will be processed at least once
- acts as a buffer between component producing data & component processing/receiving data - works well for high volume inputs or bad network requests
- Two kinds: Standard(default) and FIFO Queues
  - **Standard**
    - generally deliver messages in order they received BUT is NOT guaranteed - because of distr arch for throughput
      - messages can be out of order because of visibility timeout (see below)
      - OR because of distr arch - SQS uses multiple hosts, when SQS is polled, the answering server doesn't necessarily know all the other hosts/messages and can only provide what they have. so it may be out of order
    - nearly unlimited transactions/sec
    - DelaySeconds: when a new message is added to queue, it will be hidden from consumer inst for a fixed period
    - *can receive more than once*
  - **FIFO**
    - Order is preserved
    - NO DUPs - no duplicates because of Exactly Once processing - will keep message until consumer process deletes it
    - limit of ~ 300 transactions/sec
    - supports message groups
- *SQS is always pull based not push based*

### SQS Polling ###
- SQS requires you to "pull" data out by "polling" using the SQS API 
- messages stay in the queue anywhere from 1 min -> 14 days (4 days default)
- Visibility Timeout: amount of time message is "invisible" in SQS queue after it has been picked up
  - Ideally: after job processes it will delete the message. 
  - BUT if timer expires before job marks complete - message will become visible again and get pulled down a second time
  - THUS: need to complete job within timeout range AND delete message otherwise it will show up again in the queue
  - MAX visibility timeout is 12h
- Two Types: short and long polling
  - Short Polling - API request to ask for a message and returns answer immediately
  - Long Poll - doesnt return a message until message arrives in message queue or long poll times out
    - used when your queues might be empty for long periods of time, if you constantly poll, can cost $$$
    - set the ReceiveMessageWaitTimeSeconds to a number > 0 
    - WaitTimeSeconds: when consumer inst polls for new work, SQS will allow it to wait certain time for one or more messages to be available before closing the connection

### Other SQS Features ###
** S3 can use Event Notification to send notifications to 1 topic -> *SNS, SQS or Lambda* **  
  - can publish new obj created (s3:ObjectCreated), obj removal (s3:ObjectRemoved:Delete), or lost item in RRS storage
    - New object created events 
    - Object removal events
    - Restore object events
    - Reduced Redundancy Storage (RRS) object lost events
    - Replication events
- SNS Fan-out pattern: you can have multiple SQS items subscribe to SNS and perform further actions
  - SNS does the filtering of messages and can publish certain messages to certain SQS queues

- Backlog per inst: to determine your backlog per inst, start with ApproximateNumberOfMessages to determine length of SQS queue, divide by fleet's running capacity (InService) inst in Autoscaling group to get the backlog per inst
- using that above metric, you can also calculate what is an acceptable target per inst and then build a scale out policy based on that. 
  
### Amazon MQ ###
- Amazon MQ is a managed message broker service for Apache ActiveMQ that makes it easy to set up and operate message brokers in the cloud. 
- Used for messages, SQS is for task queues
- Message brokers allow different software systems–often using different programming languages, and on different platforms–to communicate and exchange information. 
- Amazon MQ reduces your operational load by managing the provisioning, setup, and maintenance of ActiveMQ, 
			 a popular open-source message broker. Connecting your current applications to Amazon MQ is easy because
- it uses industry-standard APIs and protocols for messaging, including JMS, NMS, AMQP, STOMP, MQTT, and WebSocket. 
- Using standards means that in most cases, there’s no need to rewrite any messaging code when you migrate to AWS.


## SWF - Simple Workflow Service  ##
- Workflow to coordinate tasks (human action, scripts, code, app services...)
  - combines digital envm with human processes
  - supports ||, synch or asynch tasks
- For example: placing a order on amazon.com where you enter address and card information (online checkout, distr businss processes)
- Differences from SQS:
  - *SWF is based on Task oriented API* whereas SQS is message oriented API
  - *SWF ensures one time only completion* whereas SQS sometimes can deliver message twice 
  - *SWF can track all tasks/events in an app* whereas SQS only knows whats in its own queue, you would have to design lot of stuff to track application workflow

- SWF Workflow executions can last up to 1year
- SWF Domain: collection of related workflows
- SWF Actors
  - Workflow Starters: apps that can initiate a WF
  - Deciders: controls flow of activity/task (what to do next)
  - Activity Workers: perform activity (workers)


## SNS - Simple Notification Service ##
- Push notifications to email, Text (SMS), HTTP endpoint...
- region specific web service to setup, operate and send notifications from the cloud 
  - contains Topics: themed groups users can subscribe to and get notifications
  - messages restored redundantly across multi-AZ
- Both SQS ans SNS are message based services
- You can use Amazon SNS message filtering to assign a filter policy to the topic subscription, and the subscriber will only receive a message that they are interested in.
- Value Props:
  - flexible message delivery over multiple protocols
  - instantaneous push-based delivery
  - single APIs & integration
  - pay as you go
  - point & click interface

## Elastic Transcoder ##
- media transcoder in cloud that converts media files from original to different formats for smart phones, tablets, PCs...
- charged based on time to process of media files and resolution required
   

   


# API Gateway #
- Front door to your AWS deployment (~ but not LB)
  - fully managed service allowing devs to publish, maintain/monitor and secure APIs at scale
- It can be used to communicate to Lambda, EC2  or DynamoDB...
- low cost and scales automatically
- **Can throttle API Gateway to prevent attacks**  
  - API owners can set a rate limit of 1,000 requests per second for a specific method in their REST APIs, and also configure Amazon API Gateway to handle a burst of 2,000 requests per second for a few seconds.
  - Amazon API Gateway tracks the number of requests per second. 
  - Any requests over the limit will receive a 429 HTTP response. 
- Expose HTTP endpoint to define Rest API

## Features
- Used in serverless arch - commonly in combo with DynamoDB & Lambda
  - can track usage of API Gateway through API keys..
  - can connect to cloudwatch for monitoring
  - allows versioning of APIs
- API Gateway has caching ability to improve performance and reduce latency 
  - can be customized based on ache key is built, TTL and data stored - even invalidating cache
- Can build an API by: 
  - Define API grouping
  - Define Resources and nested resource URL paths
  - for each resource define supported HTTP method (GET,POST,DELETE...) & set security
  - Point to target
- deploy API to a stage using default API gateway DNS or use your own custom alias
  
## Cross Site Scripting (XSS) Attacks ##
- browser permits scripts on webpage A to acces data on webpage B if they have same domain name (otherwise it doesn't allow access)
- can become a problem in AWS as you work with variety of public services (DNS, API Gateway, ELB, Cloudfront, S3...)
- "ERROR: origin policy cant be read at the remote resource" ---> then enable CORS
- **SOLUTION: Enable Cross Origin Resource Sharing (CORS) on API Gateway** since S3, EC2 will have different domain names
  - CORS is enforced by the client (browser)



# Kinesis #
- streaming data is data that is generated continuously from various sources and sends records in small packs (Kb)
  - Examples include: online purchases, stock prices, gaming data, Uber, IoT...
- Kinesis is a platform on AWS to send streaming data to, then process and analyze the data

### Kinesis Streams ###
- designed for RT processing of streaming data, by ordering them, replay records, and makes easier for counting/agg, filtering 
  - think of a factory where multiple lines dump stuff into a single slow conveyor belt. You can have multiple people sifting through looking at things... 
  - log and data feed intake
  - RT metrics and reporting on app logs
  - RT data analytics for clickstream analysis 
  - Complex stream processing
- persists push data in Shards and default retention of 1 day, up to 7 days max 
  - data pulled down by consumers and they can place output elsewhere
  - shards are grouping or pipes of data reading/output ~ 5 transactions/s for reads of up to 2 MB/s or 1000 records/s for writes of 1 MB/s
  - these shards can be strung together to create a "stream" and total capacity of the stream = sum(shards)
- data can be analyzed by EC2 and stored in S3, DynamoDB, Redshift, EMR, ECR
- because data persisted, can read the data multiple times or in batches with Lambda

- max size of data blob per record is 1MB

### Kinesis Firehose ###
- built for pushing streaming data into data stores (s3, redshift, elastisearch, splunk)
- RT data processing - data analytics on fly by lambda, data is not peristed and need to use it as it comes 
- as soon as data comes in, should be processed and results stored elsewhere
  - best way to load streaming data into S3, Redshift, Elasticsearch or Splunk

### Kinesis Analytics ###
- (perform analytics on Kinesis Streams or Firehose inside Kinesis) 
- and then stores data in S3, Redshift or Elastic Search Cluster

### Scenarios with Kinesis ###
- shard iterator expires unexpectedly when large amounts of data loaded and consumed from Kinesis
  - dynamodb table doesn't have enough capacity to store lease data
  - increase write capacity to shard table 
  - shard iterator is returned by every getrocords request as NexShardIterator 
    - iterators can expire if you don't call getrecords for more than 5 min, or you restarted consumer app 
    - so if its expiring before you can use it - kinesis ran out of write capacity

# Web Identity Federation Service is Amazon Cognito #
- lets users access AWS resources after Authc to Facebook or Google... (great for mobile apps)
- Cognito brokers between the app and Facebook or Google to provide temporary credentials which map to an IAM role to provide access to the AWS Resources
- provides seamless experience across all mobile devices
  - allows you to sign up/sign in to apps
  - create guest acc
  - synch user data across devices
  - for user authc -> NOT for access to AWS resources
- Recommended Approach:
  1. User Authc to web ID provider (facebook, google...)
  2. Successful Authc -> Cognito User Pools assign JWT back to user
  3. User gives token to Cognito ID Pools which grants perms with IAM role
  4. User can interact with allowed AWS services
    - these are temp credentials tied to an IAM role - webapps do not store creds or perms

- You should either create a federation proxy or identity provider and then use AWS security token service to create temporary tokens. You will then need to create the appropriate IAM role for which the users will assume when writing to the S3 bucket. 

- **Cognito User Pools**
  - user registration, login, authentication and grants JSON Web Token (JWT)
  - manage signup/signin f(x) for mobile/webapps
  - can add MFA for extra security
- **Cognito Identity Pools**
  - authorizes or grant IAM role to access AWS Services 
  - provides temp AWS creds to AWS services
	
# AWS Security # 
- Bad guys use automated processes, bots, DDoS, content scrapers and fake users to gather sensitive information
- stopping them as early in their process can reduce security risk and save money on infra

### Blocking IPs ###
- can use NACLs to block specific IPs - operates at L4
- OR technically speaking - you could use Host based firewalls (iptables)
- ?? if you use ELB:
  - App LB - connection filtered by NACL @ the LB point
    - connection is denied direct access to EC2
  - Network LB - will pass connection through, but it will be blocked???

## AWS Tech Support Levels ##
- https://aws.amazon.com/premiumsupport/plans/ 

- Case Response Times:
  - Developer:
    - General Guidance <24h
    - System Impaired <12h
  - Business:
    - Developer PLUS below:
    - Production system impaired: < 4 hours 
    - Production system down: < 1 hour
  - Enterprise:
    - Business PLUS below:
    - Business-critical system down: < 15 minutes

## AWS Security Token Service STS


# Encryption :: KMS - Key Management Service & HSM  # 
- regional key managing service for encryption/decryption
  - exist only within specific region and never leave it (probably why you have to decrypt AMI volumes you want to move to another region)
  - multi-tenant & shared across clients
- can manage Customer Managed Keys (CMKs) - logical pointers to crypto key
- ideal for encrypting S3, DB connection strings, API keys
- pay per API call - list, encrypt, decrypt...
- Audit using Cloud trail
- FIPS 140-2 Level 2 - show evidence of tampering
- Allows for you to: import your own keys, disable and re-enable keys and define key management roles in IAM 
  - NOT generate keys, import keys into custom key store, migrating keys out to custom key store...

### Types of Keys: ###
- **AWS Managed** 
  - free, used by default (encryption selection default) 
  - just for your acc & rotated every 3 years
- **AWS Owned**
  - used by AWS itself for its own encryption (you can't access or change)
- **Customer Managed**
  - allow key rotation, controlled via policies, advanced audit trail of when keys were used by who...

- Symmetric Keys: 
  - same key for encrypt/decrypt - AES256
  - can generate data keys
  - can import your won keys
  - used for AWS services integrated with KMS
- Asymmetric Keys:
  - different keys for encrypt/decrypt - RSA/ECC
  - public key can be downloaded & used by users who can't call KMS
  - used to sign/verify

- **Data Encryption Key - DEK**
  - for data > 4KB - asymmetric
  - creates cipher text blob and tells KMS which CMK generated it
  - should be kept with data encrypted
  - Key ID and plaintext
  - this method allows for envelope encryption. Rather than sending the entire file on the network to be encrypted/decrypted - you send the data key and it runs a program to decrypt 

## Key Policies ##
- default policy: 1 statement granting AWS ACC root user full access to CMK  AND  enable other roles to access the CMK
  - DO NOT CHANGE THE DEFAULT - could lock yourself out
- create aliases for Key IDs so keys can rotate
  - works like openssl on cmd line - can encode files by specifying a key, decodes automatically because KMS will recognize appropriate symmetric key


## Cloud HSM - Hardware Security Model ##
- FIPS 140-2 Level 3 - physical req, tampering detection & resistant
- you manage your own keys in a single-tenant, Multi-AZ cluster dedicated to YOUR ACC
- need ability to immediately remove key material from AWS KMS and prove you have done so by independent means (KMS delete has 14 day delay)
-Requirements to audit use of keys independently of KMS / CloudTrail
  - AWS has no access to your keys 
  - uses industry standard APIs, not KMS APIs (PKCS#11, JCE, MS Crypto NG...)
  - integrated with KMS so that KMS can use HSM as the dedicated key store and can hold your custom key stores
  - when you create an AWS KMS CMK in a custom key store, AWS KMS generates and stores the non-extractable key material for the CMK in an AWS CloudHSM cluster that you own and manage.

- Cloud HSM projects ENI into your VPC
  - 1 HSM per subnet & in each AZ - min of 2 AZs
  - need to explicitly provision HSM so it is HA



# Managing Secrets in AWS, Parameter Store & Secrets Manager #
- need a way to keep API keys, product keys, access creds... all secure and regularly rotated
- both have 4096 char limit on values, can be encrypted and referenced in Cloud formation
- Traditionally store secrets in deploy code/config or reference S3 bucket file somewhere that's universally accessible
  - BUT if you store keys in github or these locations ANYONE can get that access
  - also - when it came time to rotate, you had to update all locations of these parameters
- solution: Parameter store or Secrets manager - to retrieve info programmatically

## SSM - Systems Manager (Parameter Store) ##
- subcomponent of AWS Systems Manager (FREE!)
  - securely manage configurations and secrets in AWS
  - caching/distr of secrets to AWS resources (DB connection strings...) 
  - useful for parameters + secrets and so plaintext is default but you can encrypt
  - enable versioning, but only 1 version active at a time


  - allows separation of data from source control
  - secure serverless storage for config & secrets (license codes, API keys, passwords, DB conn strings...)
- values can be encrypted by KMS or be in plaintext 
- **How it works**
  - stored in a key-value store that are then retrieved by SSM Run and pulled down onto EC2, ECS, Lambda
  - Your app (on-prem, EC2, Lambda, ECS...) says /dev params please!
  - paramstore checks if IAM is allowed to retrieve
    - if encrypted request, also checks for KMS decrypt permission
  - with correct permissions, param store sends values back to app 

### Hierarchy Params ###
- can store params in a hierarchy up to 15 levels deep and track versions, set TTL...
- really useful to set up DEV-DB vs DEV-WebApp vs PROD-WebApp permissions to certain keys 
- Works through file system structure:
  - /dev has all dev info
  - /dev/db/MySQL has Dev MySQL info
- Can be integrated with CloudFormation
  - define params and reference in cloudformation template 


## AWS Secrets Manager ##
- costs money
- newer offering allowing rotation, management and retrieval of DB creds, API keys... 
- secrets manager is a password management system
  - encrypts by default and built to manage passwords and keys
- Use Secrets Manager if you need:
  -  password rotation
  -  random password generation
  -  cross account access

- it is possible to set up parameter store to pull secrets from secrets manager


# Lambda #
- regional Functions as a Service
- AWS Lambda is an event-driven task compute service that runs your code in response to “events” such as changes in data, website clicks, or messages from other AWS services without you having to manage any compute infrastructure.
  - you only manage your code
  - ultimate abstraction layer - sits above AWS API & App layer to provide:
    - compute service where you upload your code and AWS runs it 
    - no OS, patching or scaling required
- great for event-driven compute services "triggers"
- OR can use for HTTP requests or API requests through API Gateway 
- leads to new arch model: serverless arch - that is completely devoid of infra considerations 
- languages for Lambda:
  - Python, Node js, java, C#, Go, Ruby, Powershell...


- Priced on number of requests and duration of each request rounded up to nearest 100ms and amount of RAM allocated
- savings are considerable - can be 100x cheaper than EC2

### Lambda Components ###
- function (wrapper) script/program that runs in Lambda, it processes an event and returns a response
  - can configure basic settings like desc, mem usage, execution timeout, IAM role to execute as
- runtimes - different runtime programming languages to execute your function code
- layers - distr mechanism for libraries, custom runtimes, and other function dependencies. help manage your code indp from lambda
  - zip archive with libs, runtime or other dependencies - can be used to mange dependencies and keep deployment small 
  - used if the deployment package you are uploading to Lambda is too big - can be up to 5 layers
    - size restriction: total size of UNCOMPRESSED function & all layers < 250 MB
- event source - aws service that triggers your function 
  - can be directly invoked from:
  - Amazon Kinesis Firehose,
  - DynamoDB
  - SQS
- log streams - while lambda monitors your function and reports metrics to cloudwatch, you can annotate your function code with log statements so you can track perf of lambda function
  - during execution also manages **CONCURRENCY** : # of inst that serve requests at a given time
    - reserved concurrency: allocating specific inst to a particular function and preventing others from using it (viewed in console)
    - provisioned concurrency - if you can reserve, you can provision concurrency to scale without latency problems 
  - by default, executes within an AWS VPC
    - to access your VPC, need to provide your VPC subnet, sec group... and will use that info to set up an ENI so that the f(x) can talk to your resources

### Lambda Runtime ###
- When Lambda runs - it runs inside an AWS VPC (not your own)
  - to enable Lambda to access resources inside your VPC, you need to specify VPC subnet ID and sec group IDs to lambda
  - Lambda uses that info to set up ENIs that enable your function to connect to your VPC's AWS resources
    - this also means your VPC should have enough ENI capacity (additional private IPs) to support scaling of Lambda
    - recommended you create at least 1 subnet in each AZ for lambda so it can scale out to another AZ if the 1st one runs out of ENIs
    - otherwise you get the EC2ThrottledException

### Lambda Monitoring ###
- reports metrics through Amazon cloudwatch:
  - Invocations
  - latency
  - error counts & rates
  - duration
  - throttles
  - iterator age
  - deadlettererrors - messages that cant be processed correctly

- AWS X-ray allows you to debug whats happening with Lambda/Api Gateway
- Lambda can do functions globally, you can copy s3 bucket to another when info is added...
- Lambda scales out (not up) automatically
- Lambda functions are independent 1 event = 1 function
- Lambda functions can trigger another lambda function 1 event = x functions if function trigger other function 
  - RDS cannot trigger Lambda

- **Lambda@Edge**
  - run lambda functions to customize content that cloudfront delivers for faster response times
  - can affect (in order of request flowing through cloudfront):
    - 1) Viewer Request: after cloud front receives a request from a viewer 
    - 2) Origin Request: before cloudfront forwards request to the origin
    - 3) Origin Response: after cloudfront receives response from origin
    - 4) Viewer Response: before cloudfront forwards the response to the viewer

### Lambda Security ###
- when you create/update lambda functions that use envm variables, AWS encrypts them using KMS
  - during runtime - they are decrypted and given to the code 
  - by default - use KMS key
  - can configure encryption helpers by creating your own KMS key
    - For storing sensitive information, you can encrypt environment variable values prior to sending them to Lambda by using the console's encryption helpers (encrypt in transit). 
  - this allows devs to use envm variables without actually seeing the creds and variable values

### Step Functions ### 
-  serverless orchestration, serverless workflows...
- manages a workflow by breaking it into multiple steps, adding flow logic and tracking inputs and outputs 
  - stores an even tlog of data passed between app components to allow continuity if apps hang
- more modular code and workflows 

### Serverless offerings from AWS ###
- Aurora Serverless
- DynamoDB
- S3
- API Gateway
- Fargate 
- Lambda (duh)

## Serverless App Module ##
- Cloudformation extension for serverless Arch
  - new types of obj: Functions, APIs, Tables...
  - supports full features of cloudfront
  - allows you to pull down docker containers to run locally
  - can package and deploy code using code deploy 

# ECS - Elastic Container Services #
- 100% FREE - you pay for compute - running EC2 inst or Fargate
- highly scalable Docker container management service that allows you to run and manage distributed applications running in Docker containers
- scheduling/orchestrating containers ~ combo of K8s + AKS
- manages EC2 or Fargate Inst
  - EC2
    - ECS Agent
    - Docker Agent
    - ECS AMI
  - Fargate
    - Fargate Agent
    - Containerd 
- monitors resources and schedules deployments on avail inst by container requirements
  - can deploy/update/rollback...
- ties into AWS ecosystem with ELB, VPC, SecGroups, EBS vols, CloudTrail/CloudWatch


## ECS Components ##
- cluster: grouping of ECS resources (EC2 or Fargate Inst) ~ node pool
- task: single copy of a container in a task def ~ container
- task definition: like a docker file but for running containers in ECS - can contain multiple containers ~ pod manifest
- service: scaling options for the pod - min/max ~ deployment manifest
- container definition: claims pod makes for infra- CPU/RAM, network, storage...
- Registry: container image inventory to pull down container images

## Fargate ##
- serverless compute engine for containers - for ECS and EKS
- rather than define EC2 inst and pay for them - why not pay based on container app claims and let AWS manage your clusters deployments?
- provides isolation and security by running each pod on its own kernel 
- pretty much use fargate unless you need ec2 for: 
  - compliance
  - GPUs
  - extensive customization 
  - access to the host

## EKS & ECR ##
- **EKS** ECS with Kubernetes
  - can still point to Fargate or EC2 for compute
- **ECR** Elastic Container Registry
  - managed docker container registry in AWS
  - integrated with ECS/EKS and can connect to on-prem

- **Load Balancers & ECS**
  - App LB L7 allows dynamic host port mapping
    - allows multiple pods to be deployed on same instance so these services can be discovered and used
    - allows path based routing and priority rules
      - this means you can use the same listener port for App LB
  - Essentially functions as ingress???
  - basically preferred over Network LB or Classic LB

## ECS Security ##
- Instance Roles:
  - applies policy to all tasks running on an EC2 inst
- Task Roles:
  - applies to each indv task 


# Random Stuff  
AD Integration, SSO and S3 bucket access  
       -- At SAS AD Federated Services works with AD (uses SAML language) and Amazon , Amazon does Authorization
	      At SAS we dont use AWS Single Signon
  IAM Users
  Bucket Policy and ACL 
- Chef Config management -> AWS OpsWorks (think devops) 
  - config management system for chef and puppet
- IoT Core: lets connected devices easily and securely interact with cloud apps and other devices to build IoT stuff 
- Perfect forward secrecy: security feature for HTTP services like CloudFront and ELB
  - uses unique random session key to prevent decoding of captured data even if secret LT key is compromised
  - used for encryption in transit
- Server Order Preference lets you configure the load balancer to enforce cipher ordering, providing more control over the level of security used by clients to connect with your load balancer.
- The new Predefined Security Policy simplifies the configuration of your load balancer by providing a recommended cipher suite that adheres to AWS security best practices. The policy includes the latest security protocols (TLS 1.1 and 1.2), enables Server Order Preference, and offers high security ciphers such as those used for Elliptic Curve signatures and key exchanges.
- AWS CodeDeploy
  - Lambda & ECS deploys
    - Canary - define % of traffic going to new lambda function at first increment and second (all remaining traffic)
      - use when you want to define traffic shifted and at what time interval
    - Linear - linear traffic shifted every X minutes for Y% of traffic
    - All at once
  - EC2 deploys
    - Blue/Green
- apparently cloudfront servicing static/dynamic content can help with DDoS? 
  - because it uses edge locations to provide greater fault tolerance and increased scale when managing large volumes of traffic

- AWS Backup - centralized backup service that makes it easy and cost effective to backup app data 
  - central place to configure and audit resources you want to backup, automate scheduling, set retention policies and monitor backup/restore activity
  - 
  - Works with:
    - Aurora
    - Dynamo DB
    - FSx
    - RDS
    - EBS
    - EFS
    - Storage Gateway 
    - EC2
- AWS AppSync
  - can be combined with DynamoDB for quick mobile apps to keep shared data updated in RT
  - you specify data with simple code statements, and AppSync keeps app updated in RT
  - quick easy ways to access data in DynamoDB, trigger Lambda or run Elastisearch queries
